<?php
define( 'DEFAULT_JSPOPUP_WIDTH', 545 ); // pixels
define( 'STORAGE_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR );
define( 'STORAGE_EXT', 'json' );

require_once 'class/Dijkstra.php';

/**
 * A i18n wrapper function
 *
 * @param string $string
 * @return string
 */
function _esc( $string ) {
	// TODO implement also the WP i18n (see https://codex.wordpress.org/I18n_for_WordPress_Developers)
	// return is_wp () ? __ ( $string, 'wpmybackup' ) : _ ( $string );
	return _( $string );
}

/**
 * Returns a list of files by pattern contained within specified directory
 *
 * @param string $dir The root folder to scan
 * @param string $pattern The regex pattern used for scanning
 * @param bool $recursively If true then scan recursively
 * @param bool $add_empty_dir If true then add directory name to result even if it doesn't include any children
 * @param bool $tree If true then return an tree array otherwise a single array
 * @param int $output_style When 1 then includes only directory names, when 0 include both, when 2 include only file names
 * @param array $skip_files An array with the full path of files/folder to skip from scanning
 * @param bool $skip_links True when the file links are ignored, false otherwise
 * @return array Returns an array with the files/folders
 */
function getFileListByPattern( 
	$dir, 
	$pattern, 
	$recursively = false, 
	$add_empty_dir = true, 
	$tree = true, 
	$output_style = false, 
	$skip_files = null, 
	$skip_links = true ) {
	$dir .= DIRECTORY_SEPARATOR != substr( $dir, - 1 ) ? DIRECTORY_SEPARATOR : '';
	
	if ( ! file_exists( $dir ) )
		throw new Exception( sprintf( _( "File or directory %s does not exists" ), $dir ) );
	
	$dh = @opendir( $dir );
	
	( $tree && $dirname = basename( $dir ) ) || $dirname = $dir;
	
	( $tree && $files = array( $dirname => array() ) ) || ( $files = 2 == $output_style ? array() : array( $dirname ) );
	
	if ( false !== $dh ) {
		while ( false !== ( $filename = @readdir( $dh ) ) ) {
			if ( $filename == "." || $filename == ".." )
				continue;
			
			$fullPath = $dir . $filename;
			if ( ( ! empty( $skip_files ) && in_array( $fullPath, $skip_files ) ) ||
				 ( $skip_links && is_link( $fullPath ) ) )
				continue;
			
			if ( ! is_dir( $fullPath ) ) {
				if ( 1 != $output_style && ( null == $pattern || preg_match( $pattern, $fullPath ) ) )
					( $tree && $files[$dirname][] = $filename ) || $files[] = $dirname . $filename;
			} elseif ( $recursively ) {
				$children = getFileListByPattern( 
					$fullPath, 
					$pattern, 
					$recursively, 
					$add_empty_dir, 
					$tree, 
					$output_style, 
					$skip_files, 
					$skip_links );
				if ( $add_empty_dir || 1 == $output_style || ! empty( $children ) )
					( $tree && $files[$dirname] = array_merge( $children, $files[$dirname] ) ) ||
						 $files = array_merge( $children, $files );
			}
		}
		
		closedir( $dh );
	}
	array_sort_recursive( $files );
	
	return $add_empty_dir || ! empty( $files ) ? $files : '';
}

/**
 * Encodes a filename to a shorter representation on Base 32
 *
 * @param string $name The filename
 * @return string The encoded filename
 */
function encode_filename( $name ) {
	$name = explode( DIRECTORY_SEPARATOR, $name );
	array_walk( $name, function ( &$item ) {
		$item = base_convert( crc32( $item ), 10, 32 );
	} );
	
	return implode( '.', $name );
}

/**
 * Translates an encoded filename into a decoded fullpath filename
 *
 * @param string $encoded_name
 * @param array $array An array of file returned by getFileListByPattern
 * @param string $parent
 * @return string Returns the decoded fullpath filename
 */
function decode_filename( $encoded_name, &$array, $parent = '' ) {
	$result = '';
	foreach ( $array as $key => $children ) {
		$id = is_array( $children ) ? $key : $children;
		$name = $id;
		$id = ( empty( $parent ) ? '' : $parent . '.' ) . encode_filename( $id );
		
		if ( '' != ( $result = $id == $encoded_name ? $name : '' ) )
			break;
		if ( is_array( $children ) && '' != ( $result = decode_filename( $encoded_name, $children, $id, $tree ) ) ) {
			$result = $name . DIRECTORY_SEPARATOR . $result;
			break;
		}
	}
	return $result;
}

/**
 * Decode the filename of the entire list of $encoded_files
 *
 * @param array $encoded_files An array of encoded filenames
 * @param array $array An array of file returned by getFileListByPattern
 * @param bool $tree True if the specified $array is tree, false otherwise
 * @return Returns an array with the decoded filenames (the key is the encoded filename, value contains the decoded
 *         filename)
 */
function decode_filenames( &$encoded_files, &$array, $tree = true ) {
	$result = array();
	// prepare the encoded $array
	if ( ! $tree ) {
		$array1 = array();
		foreach ( $array as $item ) {
			$file = explode( DIRECTORY_SEPARATOR, $item );
			array_walk( $file, function ( &$item ) {
				$item = encode_filename( $item );
			} );
			$encoded = implode( '.', $file );
			$array1[$encoded] = $item;
		}
	}
	
	foreach ( $encoded_files as $encoded_file )
		if ( $tree )
			$result[$encoded_file] = decode_filename( $encoded_file, $array, $tree );
		else
			isset( $array1[$encoded_file] ) && $result[$encoded_file] = $array1[$encoded_file];
	
	return $result;
}

/**
 * Generates a specific JavaScript function call that will display a HTML popup window
 *
 * @param string $msg The message to show
 * @param string $quote_enclosed When true the quotes are escaped, false otherwise
 * @param string $reuse_div Whether to destroy the popup DIV or to reuse it (it will be just hidden)
 * @param string $title The caption of the window title
 */
function echoHelp( $msg, $quote_enclosed = true, $reuse_div = false, $title = null ) {
	if ( ! preg_match( "/^\'.*\'$/", $msg ) )
		$msg = "'$msg'";
	echo getHelpCall( $msg, $quote_enclosed, $reuse_div, $title );
	// /(\$help_\d+[^"]+")(.*)<br>(.*)(";$)/m find all help str that contains <br>
}

/**
 * Generates a specific JavaScript function call that will display a HTML popup window
 *
 * @param string $msg The message to show
 * @param string $quote_enclosed When true the quotes are escaped, false otherwise
 * @param string $reuse_div Whether to destroy the popup DIV or to reuse it (it will be just hidden)
 * @param string $title The caption of the window title
 */
function getHelpCall( $msg, $quote_enclosed = true, $reuse_div = false, $title = null ) {
	return ( $quote_enclosed ? '"' : '' ) . "popupWindow('" . ( empty( $title ) ? _esc( 'Help' ) : $title ) . "'," . $msg .
		 ',' . DEFAULT_JSPOPUP_WIDTH . ( $reuse_div ? 'null,null,' . $reuse_div : '' ) . ");" .
		 ( $quote_enclosed ? '"' : '' );
}

/**
 * Finds all PHP files specified in the include|require of the $filename
 *
 * @return array Returns an array of included files
 */
function getIncludeFiles( $filename ) {
	$result = array();
	$buffer = file_get_contents( $filename );
	if ( preg_match_all( '/^\s*(require|include)(_once)?\s+[^\'"]*[\'"](\w.*)[\'"]/m', $buffer, $matches ) )
		$result = $matches[3];
	array_walk( 
		$result, 
		function ( &$item ) {
			$item = preg_match( '/.*[\'"](.*)/', $item, $matches ) ? $matches[1] : basename( $item );
		} );
	return $result;
}

/**
 * Get an array of PHP files required to run some other PHP script
 *
 * @param array $files Array of files to scan
 * @param Dijkstra $obj
 * @param int $infinity The value which idicates that there is no edge from A to B (is infinity)
 * @return array Returns an array of files required by other file
 */
function get_requires( &$files, &$obj, $infinity ) {
	$tbl = $obj->getLookupTbl();
	$matrix = array();
	
	foreach ( $files as $file ) {
		$from = basename( $file );
		$data = array_filter( 
			$obj->traverse( $from, true ), 
			function ( $a ) use(&$infinity ) {
				return $a['w'] > 0 && $a['w'] < $infinity;
			} );
		$file_matrix = array();
		
		// computes the file's dependency chanin
		foreach ( $data as $to => $value )
			if ( false !== ( $to = array_search( $to, $tbl ) ) && $from != $to )
				$file_matrix[$to] = implode( ',', array_keys( $obj->getShortPath( $from, $to ) ) );
		$matrix = array_merge( $matrix, $file_matrix );
	}
	return $matrix;
}

/**
 * Get the PHP files that use a specific PHP file
 *
 * @param array $files Array of files to scan
 * @param Dijkstra $obj
 * @param int $infinity The value which idicates that there is no edge from A to B (is infinity)
 * @return array Returns an array of files used by other file
 */
function get_usedby( &$files, &$obj, $infinity ) {
	$tbl = $obj->getLookupTbl();
	$nodes = $obj->getNodes();
	$matrix = array();
	
	foreach ( $nodes as $node ) {
		$from = basename( array_search( $node, $tbl ) );
		$data = array_filter( 
			$obj->traverse( $node ), 
			function ( $a ) use(&$infinity ) {
				return $a['w'] > 0 && $a['w'] < $infinity;
			} );
		
		foreach ( $files as $file ) {
			$to = basename( $file );
			if ( $from == $to )
				continue;
			
			$file_key = $tbl[$to];
			
			isset( $data[$file_key] ) && $matrix[$from] = $obj->getShortPath( $from, $to );
		}
	}
	array_walk( $matrix, function ( &$item ) {
		$item = implode( ',', array_keys( $item ) );
	} );
	return $matrix;
}

/**
 * Check the $file include|require dependencies
 *
 * @param string $initial
 * @param array $dirlist
 * @param int $what When 0 returns those files parameter $files depends on. When 1 exactly oposite. When 2 then both.
 * @return array Returns an array containing all files that $file depends on
 */
function get_dependencies( $files, &$dirlist, $what = 0 ) {
	$infinity = pow( count( $dirlist ), 2 );
	$obj = new Dijkstra( true, $infinity ); // a directed graph
	                                        
	// add graph edges (each file is an edge in graph)
	foreach ( $dirlist as $filename ) {
		$includes = getIncludeFiles( $filename );
		foreach ( $includes as $include )
			( $filename = basename( $filename ) ) && preg_match( '/.*\.(php|fix)$/', $include ) &&
				 $obj->addEdge( $filename, $include );
		if ( 0 == count( $includes ) && preg_match( '/.*\.(php|fix)$/', $filename ) )
			$obj->addEdge( basename( $filename ), '' );
	}
	
	$result = array();
	0 == $what % 2 && $result = get_requires( $files, $obj, $infinity );
	$what >= 1 && $result = array_merge( $result, get_usedby( $files, $obj, $infinity ) );
	return $result;
}

/**
 * Creates the graph dependecy structure for $files
 *
 * @param array $files
 * @param array $dirlist
 * @return array
 */
function graph_dependencies( $files, &$dirlist ) {
	$matrix = get_dependencies( $files, $dirlist, 0 );
	file_put_contents('/tmp/uuu', print_r($matrix,1));
	$nodes = array();
	$edges = array();
	foreach ( $matrix as $key => $deps ) {
		$array = explode( ',', $deps );
		for ( $i = 1; $i < count( $array ); $i += 1 ) {
			$from = $array[$i - 1];
			$to = $array[$i];
			
			! in_array( $from, $nodes ) && $nodes[] = $from;
			! in_array( $to, $nodes ) && $nodes[] = $to;
			! empty( $from ) && ! empty( $to ) && ! isset( $edges[$from . $to] ) && $edges[$from . $to] = array( 
				$from, 
				$to );
		}
	}
	
	return array( 'nodes' => $nodes, 'edges' => array_values( $edges ) );
}

/**
 * Generates a HTML error span element
 *
 * @param string $message The error message
 * @param string $color The message color
 * @param bool $status When true the message will be prefixed with "Status"
 * @return string
 */
function formatError( $message, $color = null, $status = false ) {
	return '<span class="caption">' . ( $status ? 'Status : ' : '' ) .
		 ( empty( $color ) ? '' : '</span><span style="color:' . $color . '">' ) . $message . '</span>';
}

/**
 * Checks if the feedback form params are valid before sending the message
 *
 * @param array $params
 * @return boolean Returns true if the feedback fields are valid, false otherwise
 */
function validateSupportForm( $params ) {
	$error = null;
	if ( ! is_array( $params ) )
		die( formatError( 'Invalid message content', '#f00' ) );
	elseif ( empty( $params['email'] ) || ! preg_match( 
		'/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', 
		$params['email'] ) )
		$error = 'e-mail address';
	elseif ( empty( $params['sender'] ) || strlen( $params['sender'] ) < 3 )
		$error = 'contact name';
	elseif ( empty( $params['body'] ) || strlen( $params['body'] ) < 30 ||
		 preg_match_all( '/\s/', $params['body'], $matches ) < 4 )
		$error = 'message';
	
	if ( ! empty( $error ) ) {
		die( 
			formatError( 
				'Please give me a more descriptive <b>' . $error . '</b>.<br>The more info details, the better!', 
				'red' ) );
	} else
		return true;
}

/**
 * Sends the feedback form as an email
 *
 * @param array $method Array of feedback form fields
 */
function processSupportForm( &$method ) {
	if ( true !== validateSupportForm( $method ) )
		return;
	
	$subject = 'Feedback ' . $_SERVER['SERVER_NAME'];
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	
	$message = sprintf( 
		"New feedback received from %s, e-mail <%s>(%s)\r\n%s", 
		$method['sender'], 
		$method['email'], 
		$ip, 
		$method['body'] );
	
	// send the email; note that mail function requires proper configuration in php.ini
	$mail_sent = @mail( 'eugenmihailescux@gmail.com', $subject, $message );
	
	$likely_err = print_r( error_get_last(), true );
	// if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
	$send_status = $mail_sent ? "Your message was sent successfuly" : ( empty( $likely_err ) ? "Your message could not be sent.<br>Mail failed (I don't know exactly why)" : $likely_err );
	
	if ( isset( $send_status ) ) {
		echo formatError( $send_status, ( $mail_sent ? 'green' : 'red' ) );
	}
}

/**
 * Return a list of PHP classes declared by $filename
 *
 * @param string $filename The PHP filename to parse
 * @return array Return an array of PHP classes
 */
function fileIsClass( $filename ) {
	$result = array();
	if ( preg_match( '/.*\.php(\.fix)?/', $filename ) ) {
		if ( false != preg_match_all( 
			'/\s*((abstract|final)\s+)?class\s+\b(\w+)\b.*{/', 
			file_get_contents( $filename ), 
			$matches ) ) {
			$result = $matches[3];
		}
	}
	
	return $result;
}

/**
 * Sort the $array recursively
 *
 * @param array $array
 */
function array_sort_recursive( &$array ) {
	foreach ( $array as $key => $item )
		is_array( $item ) && array_sort_recursive( $array[$key] );
	asort( $array );
}

/**
 * Returns the project information from project's JSON file
 *
 * @param string $filename The project's JSON filename
 * @param bool $ignore_errors When true and if the project file doesn't exist the script exits, otherwise don't
 * @return array
 */
function getProjectDataByFilename( $filename, $ignore_errors = false ) {
	is_file( $filename ) || $ignore_errors ||
		 die( sprintf( 'The project\'s file "%s" does not exists or access denied.', $filename ) );
	return json_decode( file_get_contents( $filename ), true );
}

/**
 * Returns the project information by project name
 *
 * @param string $project_name The project name
 * @param bool $ignore_errors When true and if the project file doesn't exist the script exits, otherwise don't
 * @return array
 */
function getProjectData( $project_name, $ignore_errors = false ) {
	$filename = STORAGE_PATH . $project_name;
	! preg_match( '/.*\.' . STORAGE_EXT . '$/', $filename ) && $filename .= '.' . STORAGE_EXT;
	
	return getProjectDataByFilename( $filename, $ignore_errors );
}

/**
 * Scans the file from $path
 *
 * @param string $path The path to scan
 * @param bool $tree When true it returns an multi-dimensional array, when false a 0-indexed array
 * @return array
 */
function getFiles( $path, $tree = false ) {
	return getFileListByPattern( 
		$path, 
		'/[_a-z].*\.(php|fix|txt|mo|po|css|js|htm[l]|png|gif|jpg?)$/i', 
		true, 
		! $tree, 
		$tree, 
		2 );
}

/**
 * Returns the project's files with filenames decoded
 *
 * @param string $path The project files path
 * @param string $encoded_filelist A comma-delimited list of encoded file names
 * @return Returns array
 */
function getProjectFiles( $path, &$encoded_filelist ) {
	// decode the files as stored withing .json file
	$dirlist = getFiles( $path );
	$root = dirname( $path ) . DIRECTORY_SEPARATOR;
	array_walk( $dirlist, function ( &$item ) use(&$root ) {
		$item = str_replace( $root, '', $item );
	} );
	$files = explode( ',', $encoded_filelist );
	$files = decode_filenames( $files, $dirlist, false );
	$root = $path;
	$path1 = basename( $root ) . DIRECTORY_SEPARATOR;
	array_walk( $files, function ( &$item ) use($path1 ) {
		$item = str_replace( $path1, '', $item );
	} );
	asort( $files );
	return $files;
}

/**
 * Generates a PHP autoloader script for all files/classes within project
 *
 * @param string $root The PHP project root path
 * @param array $decoded_filelist The list of project files
 * @param boolean $gen_constants When true the generated PHP script will include also the path constant definitions, false otherwise
 * @param string $namespace The PHP namespace that encapsulate the PHP project
 * @return string Returns the PHP script
 */
function getProjectAutoloader( $root, &$decoded_filelist, $gen_constants = false, $namespace = '' ) {
	$classes_path = array();
	
	$def_constants = array( 'ROOT_PATH' => "' . $root . '" );
	
	// iterate all files within project, if they are classes then they're autoloaded
	foreach ( $decoded_filelist as $filename ) {
		$class_name = fileIsClass( $root . $filename );
		$prefix = strtoupper( basename( dirname( $filename ) ) );
		$classpath_const = ( '.' == $prefix ? 'ROOT' : $prefix ) . '_PATH';
		$parts = explode( DIRECTORY_SEPARATOR, dirname( $filename ) );
		for ( $i = 0; $i < count( $parts ); $i++ ) {
			$c = ( 0 == $i ? 'ROOT' : strtoupper( $parts[$i - 1] ) ) . '_PATH';
			$prefix = strtoupper( $parts[$i] );
			$d = ( '.' == $prefix ? 'ROOT' : $prefix ) . '_PATH';
			! isset( $def_constants[$d] ) && $def_constants[$d] = $c . " . '" . $parts[$i] . "'";
		}
		if ( ! empty( $class_name ) )
			foreach ( $class_name as $c )
				$classes_path[$c] = "\t\t'$c' => " . $classpath_const . " . '" . basename( $filename ) . "'";
	}
	array_walk( 
		$def_constants, 
		function ( &$item, $key ) {
			$item = "! defined ( '$key' ) && define ( '$key' , $item . DIRECTORY_SEPARATOR );";
		} );
	ksort( $classes_path );
	$classes_path_var = '$classes_path_' . crc32( empty( $namespace ) ? implode( ',', $def_constants ) : $namespace );
	$content = array( 
		'<?php', 
		'// DO NOT MODIFY IT. IT IS RECREATED AUTOMATICALLY AT EACH UPDATE.', 
		'// Generated automatically on ' . date( 'Y-m-d, H:i:s (e)', time() ), 
		'// https://bitbucket.org/eugenmihailescu/php-project-tree', 
		// '! defined ( "ROOT_PATH" ) && define ( "ROOT_PATH" , "' . $root . '" );',
		$namespace ? 'namespace ' . $namespace . ';' : '', 
		$gen_constants ? implode( PHP_EOL, $def_constants ) : '', 
		'global ' . $classes_path_var . ';', 
		$classes_path_var . ' = array (', 
		implode( ',' . PHP_EOL, $classes_path ), 
		');', 
		'', 
		'spl_autoload_register ( function ($class_name) {', 
		'global ' . $classes_path_var . ';', 
		"\t" . '$class_name = preg_replace ( "/" . __NAMESPACE__ . "\\\\\\\\/", "", $class_name );', 
		"\t" . 'isset ( ' . $classes_path_var . ' [$class_name] ) && include_once ' . $classes_path_var .
			 ' [$class_name];});', 
			'?>' );
	return $content;
}

/**
 * Pushes the filename to the client's browser
 *
 * @param string $filename The file alias as seen by browser
 * @param string $content The file content
 * @param string $mime_type The file's mime-type as seen by browser
 * @return boolean Returns false if error, othersise none
 */
function downloadFileContent( $filename, $content, $mime_type = 'application/octet-stream' ) {
	if ( headers_sent() || ! isset( $content ) || empty( $filename ) )
		return false;
	
	header( 'Expires: 0' );
	header( "Cache-Control: no-store, no-cache, must-revalidate" );
	header( "Cache-Control: post-check=0, pre-check=0", false );
	header( "Pragma: no-cache" );
	header( 'Content-Description: File Transfer' );
	header( "Content-Type: $mime_type" );
	header( "Content-disposition: attachment; filename=\"" . basename( $filename ) . "\"" );
	header( "Content-Transfer-Encoding: Binary" );
	header( 'Content-Length: ' . strlen( $content ) );
	die( $content );
}

/**
 * Pushes the filename to the client's browser
 *
 * @param string $filename The absolute path of file
 * @param string $mime_type The file's mime-type as seen by browser
 * @return boolean Returns false if error, othersise none
 */
function downloadFile( $filename, $mime_type = 'application/octet-stream' ) {
	if ( headers_sent() || ! file_exists( $filename ) )
		return false;
	
	$content = file_get_contents( $filename );
	downloadFileContent( $filename, file_get_contents( $filename ), $mime_type );
}

/**
 * Removes a directory recursively
 *
 * @param string $dir The absolute path
 * @param bool $recursive When true removes the directory recursively,false otherwise
 * @return bool Returns true if all files/subdirectories deleted successfully, false otherwise
 */
function _rmdir( $dir, $recursive = true ) {
	$files = getFileListByPattern( $dir, '/.+/', $recursive, true, false );
	rsort( $files );
	return array_reduce( 
		$files, 
		function ( $carry, $item ) {
			return $carry && ( is_dir( $item ) && rmdir( $item ) || unlink( $item ) );
		}, 
		true );
}

function strToBool( $str ) {
	return 1 === preg_match( '/(true|on|1|yes)/i', $str );
}
?>