var OPENED_POPUP_WINDOWS = [];

function getStyle(el, prop) {
	if (typeof getComputedStyle !== 'undefined') {
		return getComputedStyle(el, null).getPropertyValue(prop);
	} else {
		return el.currentStyle[prop];
	}
}
function convertEm2Pixels(value) {
	var v = /\d+/.exec(getStyle(document.documentElement, ie < 9 ? 'fontSize' : 'font-size'));
	return value * parseFloat(v);
}

var isNull = function(obj, value) {
	return (typeof obj === 'undefined' || obj === null) ? value : obj;
}

// returns IE version
var ie = (function() {

	var undef, v = 3, div = document.createElement('div'), all = div.getElementsByTagName('i');

	while (div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->', all[0])
		;

	return v > 4 ? v : undef;

}());

function popupCalcSize(id, max_width, max_height) {
	var newdiv = document.getElementById(id);

	if (!newdiv)
		return false;

	var WPadminBar = document.getElementById('wpadminbar'), WPadminMenu = document.getElementById('adminmenuback'), newdiv = document.getElementById(id);
	var titlebar = document.getElementById('titlebar_' + id), titlebarHeight = titlebar ? titlebar.clientHeight : convertEm2Pixels(2)/* em */, titlebarPadding = 2/* px */;
	var WPadminBarHeight = WPadminBar ? WPadminBar.clientHeight : 0, WPadminMenuWidth = WPadminMenu ? WPadminMenu.clientWidth : 0;

	var style = 'width:100%;', left, top;
	window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

	if (newdiv.clientHeight > window_height - 100 - WPadminBarHeight) {
		var h = window_height - WPadminBarHeight - 50;
		style += 'overflow-y:auto;max-height:' + (h - titlebarHeight - WPadminBarHeight - 24) + 'px;';

		// newdiv.style.top = "50px";
		newdiv.style.maxHeight = h + 'px';
	}
	if (newdiv.clientWidth > window_width - 100 - WPadminMenuWidth) {
		var w = window_width - 50;
		style += 'overflow:auto;max-width:' + (w - 10 - WPadminMenuWidth) + 'px;';
		// newdiv.style.left = (WPadminMenuWidth + 50) + "px";
		newdiv.style.maxWidth = w + 'px';
	}
	if (style.length > 0)
		newdiv.innerHTML = newdiv.innerHTML.replace(/title=(['"]*)@INNER-STYLE@\1/, 'style="' + style + '"');

	left = (window_width - isNull(max_width, newdiv.clientWidth) + WPadminMenuWidth) / 2;
	if (left < 0)
		left = 0;
	newdiv.style.left = left + 'px';
	top = (window_height - isNull(max_height, newdiv.clientHeight) + WPadminBarHeight) / 2;
	if (top < 0)
		top = 0;
	newdiv.style.top = top + 'px';
	
	if (null !== isNull(max_width, null))
		newdiv.style.maxWidth = isNull(max_width + 'px', newdiv.style.maxWidth);
	if (null !== isNull(max_height, null))
		newdiv.style.maxHeight = isNull(max_height + 'px', newdiv.style.maxHeight);
	newdiv.style.minWidth = 100 + "px";
}

function _addEventListener(object, type, listener, useCapture) {
	return 'function' == typeof object.addEventListener ? object.addEventListener(type, listener, useCapture) : object.attachEvent(type, listener);
}

_addEventListener(document, (ie < 9 ? 'on' : '') + 'keydown', function(ev) {
	ev = ev || window.event;
	if (ev.keyCode == 27 && (divId = Object.keys(OPENED_POPUP_WINDOWS).pop()) && (top_div = document.getElementById(divId))) {

		removePopup(top_div);

		if (ev.preventDefault)
			ev.preventDefault();
		else
			ev.returnValue = false;
		return false;
	}
});

var removePopup = function(popup) {
	if (popup) {
		var id = popup.id;
		document.body.removeChild(popup);
		popup = null;

		if (OPENED_POPUP_WINDOWS[id])
			remove_last_graph();

		delete OPENED_POPUP_WINDOWS[id];
	}
}

var removePopupLast = function() {
	var keys = Object.keys(OPENED_POPUP_WINDOWS);
	if (keys.length > 0)
		removePopup(document.getElementById(keys.pop()));
}
function removePopupAll() {
	var keys = Object.keys(OPENED_POPUP_WINDOWS);
	while (keys.length > 0)
		removePopup(document.getElementById(keys.pop()));
}
function popupWindow(title, content, width, height) {
	var zindex = 1000, titlecolor = '#2ca8d2';
	uuid = s4() + s4() + s4() + s4();
	newdiv = document.createElement('div');
	divIdName = 'popupDiv_' + uuid;
	newdiv.setAttribute('id', divIdName);

	var titlebar = document.getElementById('titlebar_' + uuid), titlebarHeight = titlebar ? titlebar.clientHeight : convertEm2Pixels(2)/* em */, titlebarPadding = 2/* px */;

	// create the div HTML content
	var s = '<div id="' + divIdName + '_close" class="popup_close" title="Click or ESC to close" onclick="myApp.removePopup(this.parentElement);"></div>';
	s += '<div id="' + divIdName.replace('popupDiv_', 'titlebar_') + '" style="background-color:' + titlecolor + ';height:' + titlebarHeight + 'px;"><table style="display:block;position:absolute;background-color:' + titlecolor + ';height:' + titlebarHeight + 'px;"><tr><td style="font-weight:bold;color:#fff;height:' + (titlebarHeight - 2 * titlebarPadding) + 'px">' + title + '</td></tr></table></div>';
	s += '<div id="' + divIdName + '_inner" class="popup-inner" title="@INNER-STYLE@" onmousedown="return"><table style="width:100%;"><tr><td style="padding:4px;">' + content + '</td></tr>';
	s += '</table></div>';
	newdiv.innerHTML = s;

	newdiv.style.position = "fixed";
	newdiv.style.padding = "5px";
	newdiv.style.border = "2px solid";
	newdiv.style.borderColor = "#a1a1a1";
	newdiv.style.borderRadius = "10px";
	newdiv.style.boxShadow = "10px 10px 5px rgb" + (ie < 9 ? "" : "a") + "(0,0,0" + (ie < 9 ? "" : ",0.5") + ")";
	newdiv.style.background = "white";
	newdiv.style.color = "black";
	newdiv.style.select = 'none';
	newdiv.style.zIndex = zindex ? zindex : 1000;

	// inject the HTML content inside DIV
	document.body.appendChild(newdiv);

	OPENED_POPUP_WINDOWS[divIdName] = true;

	popupCalcSize(divIdName, width, height);

	return newdiv;
}