function toggle_feedbackbox() {
	var feedback_box = document.getElementById('feedback_box');
	if (feedback_box.style.left === 0 || feedback_box.style.left === '0px') {
		feedback_box.setAttribute('style', 'left:-' + feedback_box.offsetWidth + 'px');
	} else {
		feedback_box.setAttribute('style', 'left:0px');

		_addEventListener(document, (ie < 9 ? 'on' : '') + 'keydown', function(ev) {
			ev = ev || window.event;
			if (ev.keyCode == 27) {

				toggle_feedbackbox();

				if (ev.preventDefault)
					ev.preventDefault();
				else
					ev.returnValue = false;
				return false;
			}
		});

	}
}

function sendFeedback() {
	var formData = new FormData(), fields = [ 'sender', 'email', 'body' ], i;
	formData.append('action', 'send_feedback');
	for (i = 0; i < fields.length; i += 1) {
		formData.append(fields[i], document.getElementById(fields[i]).value);
	}
	onready = function(xmlhttp) {
		popupWindow('Sent status', xmlhttp.responseText)
	};
	ajaxRequest(location.href, formData, onready);
}
if (ie < 9) {
	if (!document.cookie.match("ie9=true")) {
		alert("The feedback form is hardly supported on IE<9.\nFor the best experience go with FF/Chrome.");
		document.cookie = "ie9=true";
	}
}
