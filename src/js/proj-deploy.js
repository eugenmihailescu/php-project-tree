function getCookie(cname) {
	var ca = document.cookie.split(';');
	var pair = null, found = false;
	for (var i = 0; !found && i < ca.length; i++) {
		pair = (ca[i].trim()).split('=');
		if (pair && pair[0] == cname)
			found = true;
	}
	return found && pair && pair.length > 1 ? pair[1] : null;
}

function setCookie(cname, cvalue, exdays, force) {
	force = 'undefined' != force ? force : false;
	if (!force && 'false' == getCookie('cookie_accept')) {
		return;// sorry, this user does not like cookies (try coke instead
		// ;-)
	}
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}
function color2hex(c) {
	var r = new RegExp(/rgb[a]?\s*\((\s*\d+\s*),(\s*\d+\s*),(\s*\d+\s*)\)/i);
	if (r = r.exec(c))
		return '#' + parseInt(r[1]).toString(16) + parseInt(r[2]).toString(16) + parseInt(r[3]).toString(16);
	else
		return c;
}
/**
 * @returns
 */
function s4() {
	return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}
var guid = (function() {
	"use strict";
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return function() {
		return s4() + s4() + '_' + s4() + '_' + s4() + '_' + s4() + '_' + s4() + s4() + s4();
	};
}());
function createDocElement(parent, type, attributes, text, addtoparent) {
	'use strict';
	if (!addtoparent) {
		addtoparent = true;// default
	}
	var prop, node, child = document.createElement(type);
	child.setAttribute('id', guid());

	if (([ 'form', 'input' ]).indexOf(type) < 0) {
		node = document.createTextNode(text || '');
	}

	if (attributes) {
		for (prop in attributes) {
			if (attributes.hasOwnProperty(prop)) {
				child.setAttribute(prop, attributes[prop]);
			}
		}
	}

	if (node) {
		child.appendChild(node);
	}

	if (addtoparent && parent) {
		parent.appendChild(child);
	}

	return child;
}
function ajaxRequest(url, params, callback, method) {
	'use strict';
	// Note: this should be used within the same domain (don't violate CORS)
	// On Firefox we only get a warning on Developer Console tool

	if (!method) {
		method = 'POST';// default
	}

	var xmlhttp, start = new Date();

	// http://en.wikipedia.org/wiki/XMLHttpRequest
	if (typeof XMLHttpRequest === 'undefined') {
		XMLHttpRequest = function() {
			try {
				return new ActiveXObject("Msxml2.XMLHTTP.6.0");
			} catch (e) {
			}
			try {
				return new ActiveXObject("Msxml2.XMLHTTP.3.0");
			} catch (e) {
			}
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
			}
			throw new Error("This browser does not support XMLHttpRequest.");
		};
	}

	xmlhttp = new window.XMLHttpRequest();

	if (callback) {
		/* jslint white: true */
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState >= 4)
				callback(xmlhttp, start);
		};
	}

	if (url) {
		try {

			xmlhttp.open(method, url, true);

			// Tells server that this call is made for ajax purposes.
			xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

			if (params && params.length > 0) {
				xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			}

			xmlhttp.send(params);
		} catch (err) {
			if (callback) {
				callback('JavaScript error: ' + err.message);
			}
		}
	} else {
		throw 'URL not specified!';
	}
	return xmlhttp;
}
function post(path, params, method) {
	'use strict';
	method = method || 'post';// POST rulz!
	path = path || location.href;

	// create a temporary form
	var form = createDocElement(document.body, 'form', { 'method' : method,
	'action' : path }, null, false), key;

	// populate it with hidden params
	for (key in params) {
		if (params.hasOwnProperty(key)) {
			createDocElement(form, 'input', { 'type' : 'hidden',
			'name' : key,
			'value' : ('object' == typeof params[key] ? JSON.stringify(params[key]) : params[key]) });
		}
	}

	document.body.appendChild(form);

	form.submit();
	document.body.removeChild(form);
	form = null;
}
function showError($msg) {
	document.getElementById('status').innerHTML = '<span style="color:red">Error: ' + $msg + '</span>';
}
function showInfo($msg) {
	document.getElementById('status').innerHTML = '<span style="color:green">' + $msg + '</span>';
}
/**
 * Initialize UI fields base on the input str
 * 
 * @param action
 *            The event that triggered this function
 * @param str
 *            The JSON string sent with the action
 * 
 * Note: When new fields are added please update this function too
 */
function do_response(action, str) {
	var file_list = document.getElementById('file_list'), wrapper = document.getElementById('wrapper');
	var window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	switch (action) {
		case 'scan':
			file_list.innerHTML = str;
			file_list.style.maxHeight = (window_height - file_list.offsetTop - 80) + 'px';
			break;
		case 'remove':
			var done = false;
			if (str.length > 0) {
				var tr = document.getElementById('t' + str);
				if (tr)
					done = tr.parentNode.removeChild(tr);
			}

			if (!done)
				showError(str);

			break;
		case 'edit':
			var proj_info_row = document.getElementById('proj_info_row'), proj_path = document.getElementById('proj_path'), pack_path = document
					.getElementById('pack_path'), group = document.getElementById('proj_group'), arc_type = document.getElementById('arc_type'), proj_name = document
					.getElementById('proj_name'), chk_tested = document.getElementById('chk_tested'), lbl_filecount = document.getElementById('lbl_filecount'), lbl_selcount = document
					.getElementById('lbl_selcount'), proj_desc = document.getElementById('proj_desc'), proj_uri = document.getElementById('proj_uri'), proj_lib = document
					.getElementById('proj_lib'), proj_author = document.getElementById('proj_author'), proj_description = document
					.getElementById('proj_description'), proj_requires = document.getElementById('proj_requires'), proj_tested = document
					.getElementById('proj_tested'), proj_donate_link = document.getElementById('proj_donate_link'), proj_contributors = document
					.getElementById('proj_contributors'), proj_sections = document.getElementById('proj_sections'), proj_license = document
					.getElementById('proj_license'), proj_tags = document.getElementById('proj_tags'), proj_banners = document.getElementById('proj_banners'), obj;
			try {
				obj = JSON.parse(str);

				file_list.innerHTML = obj.files;
				proj_path.value = obj.path;
				proj_desc.value = obj.desc;
				proj_uri.value = obj.uri;
				proj_lib.value = obj.lib;
				pack_path.value = obj.pack_path ? obj.pack_path : '';
				group.value = obj.group ? obj.group : 'default';
				arc_type.value = obj.arc_type ? obj.arc_type : 'tarbz';
				chk_tested.checked = obj.tested && '1' == obj.tested;
				proj_name.value = obj.proj;
				proj_author.value = obj.author;
				proj_description.value = obj.description;
				proj_requires.value = obj.requires;
				proj_tested.value = obj.tested_ver;
				proj_donate_link.value = obj.donate;
				proj_contributors.value = obj.contributors;
				proj_sections.value = obj.sections;
				proj_license.value = obj.license;
				proj_tags.value = obj.tags;
				proj_banners.value = obj.banners;

				lbl_filecount.innerHTML = obj.filescount;
				lbl_selcount.innerHTML = obj.selcount;
				proj_info_row.style.display = 'inline-block';
				upd_btn_status('next');
				file_list.style.maxHeight = (window_height - file_list.offsetTop - 80) + 'px';
			} catch (err) {
				showError(err + "<br>" + str);
			}
			break;
		case 'save':
			if ('1' != str)
				showError(str);
			break;
		case 'chk_dependency':
			var r = str.split(';'), c, i, p, j = 0, e = file_list.getElementsByTagName('div'), lbl_deps = document.getElementById('lbl_deps');
			for (i = 0; i < e.length; i += 1)
				if ('yellow' == e[i].style.backgroundColor || ('#ccff99' == color2hex(e[i].style.backgroundColor)) && !e[i].firstChild.checked)
					e[i].style.backgroundColor = '';

			for (i = 0; i < r.length; i += 1) {
				p = r[i].split('=');
				if (0 == p.length || 0 == p[0].length)
					continue;
				c = document.getElementById(p[0]);
				if (c && !c.parentNode.firstChild.checked) {
					if ('yellow' != c.parentNode.style.backgroundColor)
						c.parentNode.style.backgroundColor = 'yellow';
					c.parentNode.title = p[1].replace(/,/g, ' -> ');
					j++;
				}
			}
			lbl_deps.innerHTML = 0 == j ? '' : (j + ' missed');
			document.getElementById('btn_fixdeps').style.display = j ? 'inline-block' : 'none';
			break;
		case 'lst_usedby':
		case 'lst_dependency':
			var s = '', i, m, p, t, u, v, r = str.split(';');
			for (i = 0; i < r.length; i += 1) {
				p = r[i].split('=');
				if (p.length < 2 || 0 == p[0].length)
					continue;
				t = p[1].split(',');
				u = 'lst_dependency' == action ? t.length - 1 : 0;
				v = 'lst_dependency' == action ? 0 : t.length - 1;
				s += '<li><a style="cursor:help;border-bottom:1px dotted blue;" title="' + p[1].replace(/,/g, ' -> ') + '">' + t[u] + '</a></li>';
			}
			if (t) {
				m = 'lst_dependency' == action ? '<b>' + t[v] + '</b> depends on the following files' : ('The following files depends on <b>' + t[v] + '</b>');
				s = m + ':<ul>' + s + '</ul>';
				if ('lst_dependency' == action)
					s += '<input type="button" class="button" style="display:block;margin:auto" value="Files depending on ' + t[0] + '" onclick="do_action(\'lst_usedby\',\'proj=\'+document.getElementById(\'proj_name\').value+\'&id=' + t[0] + '\');">';
				popupWindow('File dependencies', s);
			}
			break;
		case 'graph_file':
		case 'graph':
			try {
				var obj = JSON.parse(str);
			} catch (err) {
				showError(err.message);
			}
			if ('graph' == action)
				upd_btn_status(action);
			if (obj)
				draw_graph(document, obj.nodes, obj.edges, 'graph_file' == action);

			break;
		case 'file_view':
			popupWindow('File viewer', '<pre>' + str + '</pre>');
			break;
		case 'deploy':
			if (str.length > 0)
				showError(str);
			break;
	}
	wrapper.className = wrapper.className.replace(' loading', '');
	document.body.style.cursor = 'default';
}
function do_action(action, params, async) {
	document.body.style.cursor = 'wait';
	async = typeof async == 'undefined' ? true : async;
	if (!async) {
		var params = params.split('&'), p, r = { action : action };
		for (var i = 0; i < params.length; i += 1) {
			p = params[i].split('=');
			r[p[0]] = p[1];
		}
		post(null, r);
	}
	var e = document.getElementById('wrapper'), l = ' loading';
	if (e.className.indexOf(l) < 0)
		e.className += l;
	ajaxRequest(location.href, 'action=' + action + (params != 'undefined' && params.length > 0 ? '&' + params : ''), function(xmlhttp) {
		do_response(action, xmlhttp.responseText);
	});
}
function upd_btn_status(action) {
	var btn_add = document.getElementById('btn_add'), btn_next = document.getElementById('btn_next'), btn_cancel = document.getElementById('btn_cancel'), btn_save = document
			.getElementById('btn_save'), proj_info_row = document.getElementById('proj_info_row'), tbl_proj = document.getElementById('tbl_proj'), file_list = document
			.getElementById('file_list'), btn_deps = document.getElementById('btn_deps'), btn_fixdeps = document.getElementById('btn_fixdeps'), btn_export = document
			.getElementById('btn_export'), btn_graph = document.getElementById('btn_graph'), btn_list = document.getElementById('btn_list'), btn_aloader = document
			.getElementById('btn_aloader'), btn_pack = document.getElementById('btn_pack'), btn_deploy = document.getElementById('btn_deploy');

	var proj_info_row_style = 'inline-block';
	var tbl_proj_style = 'none';
	var file_list_style = 'none';
	switch (action) {
		case 'add':
			btn_add.style.display = 'none';
			btn_next.style.display = 'inline-block';
			btn_cancel.style.display = 'inline-block';
			btn_save.style.display = 'none';
			btn_deps.style.display = 'none';
			break;
		case 'next':
			btn_graph.style.display = 'inline-block';
			btn_list.style.display = 'none';
			btn_add.style.display = 'none';
			btn_next.style.display = 'none';
			btn_cancel.style.display = 'inline-block';
			btn_save.style.display = 'inline-block';
			file_list_style = 'block';
			btn_deps.style.display = 'inline-block';
			break;
		case 'save':
			file_list_style = 'block';
			break;
		case 'graph':
			file_list_style = 'none';
			btn_save.style.display = 'inline-block';
			btn_list.style.display = 'inline-block';
			btn_graph.style.display = 'none';
			break;
		default:
			btn_list.style.display = 'none';
			btn_graph.style.display = 'none';
			btn_add.style.display = 'inline-block';
			btn_next.style.display = 'none';
			btn_cancel.style.display = 'none';
			btn_save.style.display = 'none';
			btn_deps.style.display = 'none';
			proj_info_row_style = 'none';
			tbl_proj_style = 'inline-block';
			break;
	}

	remove_last_graph();

	btn_export.style.display = btn_save.style.display;
	btn_aloader.style.display = btn_export.style.display;
	btn_pack.style.display = btn_export.style.display;

	btn_fixdeps.style.display = 'none';
	proj_info_row.style.display = proj_info_row_style;
	file_list.style.display = file_list_style;
	tbl_proj.style.display = tbl_proj_style;

	btn_deploy.style.display = btn_add.style.display;

	var window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	if (tbl_proj.clientHeight > window_height - 165)
		tbl_proj.style.maxHeight = (window_height - 165) + 'px';
}

/**
 * Process the UI actions
 * 
 * @param action
 *            The event that triggered this function
 * 
 * Note: When new fields are added please update this function too
 */
function add_project(action) {
	var proj_path = document.getElementById('proj_path'), pack_path = document.getElementById('pack_path'), group = document.getElementById('proj_group'), arc_type = document
			.getElementById('arc_type'), chk_tested = document.getElementById('chk_tested'), proj_name = document.getElementById('proj_name'), tbl_proj = document
			.getElementById('tbl_proj'), proj_desc = document.getElementById('proj_desc'), proj_uri = document.getElementById('proj_uri'), proj_lib = document
			.getElementById('proj_lib'), proj_author = document.getElementById('proj_author'), proj_description = document.getElementById('proj_description'), proj_requires = document
			.getElementById('proj_requires'), proj_tested = document.getElementById('proj_tested'), proj_donate_link = document
			.getElementById('proj_donate_link'), proj_contributors = document.getElementById('proj_contributors'), proj_sections = document
			.getElementById('proj_sections'), proj_license = document.getElementById('proj_license'), proj_tags = document.getElementById('proj_tags'), proj_banners = document
			.getElementById('proj_banners'), chklist;
	switch (action) {
		case 'next':
			if (0 == proj_path.value.length)
				return showError('Project path cannot be empty');
			else if (0 == proj_name.value.length)
				return showError('Project name cannot be empty');
			else
				do_action('scan', 'path=' + proj_path.value);
			break;
		case 'save':
			var files = [];
			var el = document.getElementById('file_list').getElementsByTagName('input');
			for (var i = 0; i < el.length; i += 1)
				if (el[i].checked)
					files.push(el[i].id);
			return do_action(action, 'path=' + proj_path.value + '&proj=' + proj_name.value + '&pack_path=' + pack_path.value + '&group=' + group.value + '&arc_type=' + arc_type.options[arc_type.selectedIndex].value + '&tested=' + (chk_tested.checked ? 1 : 0) + '&desc=' + proj_desc.value + '&uri=' + proj_uri.value + '&lib=' + proj_lib.value + '&files=' + files
					.join(',') + '&author=' + proj_author.value + '&description=' + proj_description.value + '&requires=' + proj_requires.value + '&tested_ver=' + proj_tested.value + '&donate=' + proj_donate_link.value + '&contributors=' + proj_contributors.value + '&sections=' + proj_sections.value + '&license=' + proj_license.value + '&tags=' + proj_tags.value + '&banners=' + proj_banners.value);
			break;
		case 'cancel':
			location.href = location.href;
			break;
		case 'deploy':
			var projlist = [];
			chklist = tbl_proj.getElementsByTagName('input');
			if ('none' == chklist[0].style.display) {
				for (var i = 0; i < chklist.length; i += 1)
					chklist[i].style.display = 'inline-block';
				var e = document.getElementById('btn_deploy');
				if (e)
					e.value += " Now";
			} else {
				var deploy_path = getCookie('deploy_path');
				if (null == (deploy_path = prompt('Enter the deployment path', deploy_path)))
					break;
				setCookie('deploy_path', deploy_path, 10);
				for (var i = 0; i < chklist.length; i += 1)
					if (chklist[i].checked && '1' != chklist[i].dataset.isgroup)
						projlist.push(chklist[i].id);
				if (0 == projlist.length)
					return showError('No item selected');
				else
					return do_action(action, 'projlist=' + projlist.join(',') + '&path=' + deploy_path);
			}
			break;
	}
	upd_btn_status(action);
}
function get_parent(name) {
	var parent = /(.*)\./.exec(name);
	return parent ? parent[1] : '';
}
function get_inheritence(root, name, siblings) {
	siblings = typeof siblings != 'undefined' ? siblings : false;
	var k = [], j = root.getElementsByTagName('INPUT'), l = siblings ? '\\w+$' : '.+', p = get_parent(name).replace(/\./g, '\\.'), r = new RegExp('^' + (siblings ? p : name) + '\\.' + l);

	for (var i = 0; i < j.length; i += 1)
		if (j[i].type == 'checkbox' && r.test(j[i].id))
			k.push(j[i].id);

	return k;
}
function count_checked() {
	var lbl_selcount = document.getElementById('lbl_selcount'), root = document.getElementById('file_list'), chks = root.getElementsByTagName('input'), i, c = 0;
	for (i = 0; i < chks.length; i += 1)
		c += 'checkbox' == chks[i].type && chks[i].checked;
	lbl_selcount.innerHTML = c;
}
function balance_checkboxes(sender) {
	if (typeof sender != 'undefined') {
		var parent = get_parent(sender.id), root = document.getElementById('file_list'), siblings = get_inheritence(root, sender.id, true), children = get_inheritence(root, sender.id), i, e, p_checked = sender.checked;

		if (parent) {
			for (i = 0; p_checked && i < siblings.length; i += 1)
				p_checked = p_checked && document.getElementById(siblings[i]).checked;
			document.getElementById(parent).checked = p_checked;
		}
		if (children)
			for (i = 0; i < children.length; i += 1)
				document.getElementById(children[i]).checked = sender.checked;

		e = sender.parentNode.getElementsByTagName('img');
		for (i = 0; i < e.length; i += 1)
			e[i].style.display = sender.checked ? 'inline-block' : 'none';
	}
	document.getElementById('btn_save').click();
	showInfo('Automatically saved at ' + Date());
	count_checked();
}
function fix_deps() {
	var file_list = document.getElementById('file_list'), divs = file_list.getElementsByTagName('div'), i;
	for (i = 0; i < divs.length; i += 1)
		if ('yellow' == divs[i].style.backgroundColor) {
			divs[i].firstChild.checked = true;
			divs[i].firstChild.onclick();
			divs[i].style.backgroundColor = '#cf9';
		}
	document.getElementById('btn_deps').click();
	balance_checkboxes();
}
