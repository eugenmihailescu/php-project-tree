var SPRINGIFY_STACK = [];

function remove_last_graph() {
	var item = window.SPRINGIFY_STACK.pop();
	if (typeof item != 'undefined') {
		var canvas = item.canvas, graph = item.graph, springy = item.springy;

		if (springy) {
			springy.renderer.stop();
			delete springy;
		}
		if (graph)
			delete graph;
		if (canvas)
			canvas.parentNode.removeChild(canvas);
	}
}
function draw_graph(parent, nodes, edges, popup) {
	popup = popup || false;
	var window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	var window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var wrapper = document.getElementById('wrapper'), graph_canvas, parent, width, height, outline = true, uniqid = guid();

	var i, j, v_nodes = [], graph = new Springy.Graph();

	// create the nodes
	for (i = 0; i < nodes.length; i += 1)
		v_nodes[nodes[i]] = graph.newNode({ label : nodes[i],
		font : 'monospace' });

	// create the edges
	for (i = 0; i < edges.length; i += 1)
		graph.newEdge(v_nodes[edges[i][0]], v_nodes[edges[i][1]], { color : '#82BBFB' });

	if (popup) {
		outline = false;
		width = 640;
		height = 440;
		removePopupAll();
		parent = popupWindow('File dependecy graph', '', 640, 480);
	} else {
		parent = document.body;
		width = window_width - 18;
		height = window_height - wrapper.clientHeight - wrapper.offsetTop - 53;
	}

	graph_canvas = createDocElement(parent, 'canvas', { id : uniqid,
	width : width,
	height : height,
	style : (outline ? "outline: 10px solid #82bbfb; " : '') + "background-color: white;font-family: monospace; font-size: 0.5em" });

	var springy = jQuery('#' + uniqid).springy({ graph : graph,
	nodeSelected : function(node) {
		do_action('graph_file', 'proj=' + document.getElementById('proj_name').value + '&id=' + node.data.label);
	} });
	window.SPRINGIFY_STACK.push({ canvas : graph_canvas,
	graph : graph,
	springy : springy });
}