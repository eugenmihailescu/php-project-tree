This project started as an experiment and finally it turned out in a useful piece of code.

I am not proud either of my coding style or the way I organized the source code. It was never intended to by something else than a scratchpad.

On a scale from 1-10 I would give it 4 for the way the PHP code is organized/written.
On a scale from 1-10 I would give it 7 for the performances.

I intenationally avoided jQuery or any other PHP/JS library except for Springy which is a project by itself.

The project can be definetely improved and hopefully if I found the time/resources needed I will make it shine.

If you are not interested in project development then all the above doesn't matter for you. 
If you are developer and intend to fork and extend this project then better use its concept/code and start something from scratch, write it the right way.

Eugen Mihailescu, 2015-12-12