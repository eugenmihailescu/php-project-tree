<?php
require_once 'MyException.php';
class LogFile {
	// private $_current_user_id;
	private $_logrotate;
	private $_logsize;
	private $_logdir;
	private $_log_filename;
	private $_filter;
	private $_branched;
	private $_branch_id;
	private $_rw_filter;
	private $_rw_mode;
	private function _getLogfile() {
		if (empty ( $this->_log_filename ) /*|| empty ( $this->_logdir )*/)
			return false;
			/*
		 * $path = (! empty ( $this->_logdir ) && substr ( $this->_logdir, - 1 ) != DIRECTORY_SEPARATOR ? $this->_logdir . DIRECTORY_SEPARATOR : $this->_logdir); $path .= ! empty ( $this->_current_user_id ) ? $this->_current_user_id . DIRECTORY_SEPARATOR : '';
		 */
		return /*$path . */$this->_log_filename;
	}
	private function _validateFilename($logfile) {
		if (false === $logfile)
			throw new MyException ( 'The log filename is empty' );
		$path = dirname ( $logfile );
		! file_exists ( $path ) && mkdir ( $path, 0770, true );
	}
	function __construct($log_file = null, $settings = null) {
		global $_branch_id_;
		$this->_logrotate = false;
		$this->_logsize = 1;
		$this->_logdir = ! empty ( $log_file ) ? dirname ( $log_file ) : sys_get_temp_dir ();
		$this->_log_filename = ! empty ( $log_file ) ? $log_file : null;
		is_array ( $settings ) && $this->initFromArray ( $settings );
		
		if (defined ( 'BRANCHED_LOGS' ) && BRANCHED_LOGS && ! empty ( $_branch_id_ )) {
			$this->_setBranched ();
		} else {
			$this->_branched = false;
			$this->_branch_id = null;
			$this->_rw_filter = null;
			$this->_rw_mode = null;
		}
	}
	private function _setBranched() {
		global $COMPRESSION_FILTERS;
		$this->_branched = true;
		$this->_rw_filter = $COMPRESSION_FILTERS [GZ] [0];
		$this->_rw_mode = sprintf ( $COMPRESSION_FILTERS [GZ] [1], 9 );
		$this->_setBranchId (); // force job filename change
	}
	private function _setBranchId() {
		global $_branch_id_;
		$log_file = $this->_getLogfile ();
		$this->_branch_id = $_branch_id_;
		$fname = basename ( $log_file );
		
		if (! $this->_branched)
			$this->_log_filename = preg_replace ( "@{$this->_branch_id}" . normalize_path ( DIRECTORY_SEPARATOR ) . "$fname$@", $fname, $log_file ); // cut the branch
		else
			$this->_log_filename = getBranchedFileName ( $log_file ); // seed the branch
		! empty ( $this->_rw_filter ) && $this->_log_filename .= '.' . $this->_rw_filter;
	}
	private function _rotateLog($log_file) {
		if (! $this->_logrotate)
			return false;
		
		if (NONE == $this->_filter) {
			$new_name = sprintf ( '%s.%s', $log_file, date ( 'Ymd-His' ) );
			$success = move_file ( $log_file, $new_name );
			if (! $success)
				return false;
			else
				return $new_name;
		}
		
		global $COMPRESSION_NAMES, $COMPRESSION_FILTERS;
		$filter = '';
		$ext = '.' . $COMPRESSION_NAMES [$this->_filter];
		
		$filter = $COMPRESSION_FILTERS [$this->_filter] [0];
		$mode = sprintf ( $COMPRESSION_FILTERS [$this->_filter] [1], 9 ); // 9=best compression
		
		if (! function_exists ( $filter . 'open' ))
			throw new MyException ( sprintf ( _esc ( '%s support is not enabled. Check your PHP configuration (php.ini) or contact your hosting provider.' ), strtoupper ( $filter ) ) );
		
		$output_file = $log_file . $ext;
		$i = 0;
		while ( file_exists ( $output_file ) )
			$output_file = sprintf ( '%s-%d', $log_file . $ext, $i ++ );
		
		if (! file_exists ( $log_file ))
			throw new MyException ( sprintf ( _esc ( "Cannot rotate log file %s due to it doesn't exist" ), $log_file ) );
		
		if ('' != $filter) {
			$fw = _call_user_func ( $filter . 'open', $output_file, $mode );
			
			$fr = fopen ( $log_file, 'rb' );
			if (false !== $fr) {
				while ( ! feof ( $fr ) )
					_call_user_func ( $filter . 'write', $fw, fread ( $fr, MB ) );
				
				fclose ( $fr );
			}
			_call_user_func ( $filter . 'close', $fw );
		}
		
		return $output_file;
	}
	public function initFromArray($array) {
		$options = array (
				'logdir' => '_logdir',
				'logrotate' => '_logrotate',
				'logsize' => '_logsize',
				'method' => '_filter',
				'current_user_id' => '_current_user_id' 
		);
		foreach ( $options as $key => $prop )
			isset ( $array [$key] ) && $this->$prop = $array [$key];
	}
	public function writeLog($str) {
		$logfile = $this->_getLogfile ();
		$this->_validateFilename ( $logfile );
		
		if ($this->_logrotate && file_exists ( $logfile ) && filesize ( $logfile ) + strlen ( $str ) > $this->_logsize * MB)
			if (false !== $this->_rotateLog ( $logfile ))
				@unlink ( $logfile );
			
			// print_r is buggy when using buffered output so use this surogate instead
		$line = is_string ( $str ) ? $str : obsafe_print_r ( $str, true );
		
		if (null == $this->_rw_filter) {
			if (false === file_put_contents ( $logfile, $line, FILE_APPEND ))
				trigger_error ( _esc ( 'Cannot write to the log file' ) . ' "' . $logfile . '"', E_USER_WARNING );
		} else {
			if (file_exists ( $logfile )) {
				$log_data = gzfile ( $logfile );
				$log_data [] = $line;
				$log_data = implode ( $log_data );
			} else
				$log_data = $line;
			
			$fw = _call_user_func ( $this->_rw_filter . 'open', $logfile, $this->_rw_mode );
			if (flock ( $fw, LOCK_EX )) {
				_call_user_func ( $this->_rw_filter . 'write', $fw, $log_data, strlen ( $log_data ) );
				flock ( $fw, LOCK_UN );
				_call_user_func ( $this->_rw_filter . 'close', $fw );
			} else
				throw new MyException ( sprintf ( _esc ( 'Cannot aquire exclusive lock for writting to %s' ), $logfile ) );
		}
	}
	public function readLog() {
		$logfile = $this->_getLogfile ();
		if (null == $this->_rw_filter)
			return file_get_contents ( $logfile );
		else {
			$log_data = '';
			$fw = _call_user_func ( $this->_rw_filter . 'open', $logfile, $this->_rw_mode );
			while ( ! feof ( $fw ) )
				$log_data .= _call_user_func ( $this->_rw_filter . 'read', $fw, 4096 );
			_call_user_func ( $this->_rw_filter . 'close', $fw );
			return $log_data;
		}
	}
	public function getLastJobId() {
		$result = false;
		if (false !== ($fr = fopen ( $this->_log_filename, 'r' ))) {
			$buff_len = min ( 4096, filesize ( $this->_log_filename ) );
			if (0 == fseek ( $fr, - $buff_len, SEEK_END ) && false !== ($buff = fread ( $fr, $buff_len ))) {
				// find the last job_id entry then step-backward until EOL found
				$key = 'job_id:';
				$p = strrpos ( $buff, $key );
				false !== $p && ($p = strrpos ( substr ( $buff, 0, $p ), PHP_EOL ));
				
				$buff = substr ( $buff, $p ); // $buff contains a substring with the last job_id line (+ eventually the lines that comes after it)
				if (preg_match ( '/\[([\d\-\s\:]+)\][^\(]+\(' . $key . '\s*([\-\d]+)\)/', $buff, $matches ))
					$result = $matches;
			}
			fclose ( $fr );
		}
		return $result;
	}
}
?>