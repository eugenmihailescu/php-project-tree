<?php
require_once 'LogFile.php';
class MyException extends \Exception {
	protected function _trigger_exception($message, $code = null, $previous = null) {
		if (defined ( 'PHP_DEBUG_ON' ) && PHP_DEBUG_ON)
			if (defined ( 'TRACE_DEBUG_LOG' )) {
				global $settings;
				$log_file = new LogFile ( TRACE_DEBUG_LOG, $settings );
				
				$log_file->writeLog ( str_repeat ( '-', 80 ) . PHP_EOL );
				$log_file->writeLog ( sprintf ( '[%s] %s (%s: %d)' . PHP_EOL, date ( DATETIME_FORMAT ), $message, _esc ( 'code' ), $code ) );
				$log_file->writeLog ( str_repeat ( '-', 80 ) . PHP_EOL );
				$trace_str = $this->getTraceAsString ();
				if (! empty ( $trace_str ))
					$log_file->writeLog ( $trace_str );
			} else
				trigger_error ( _esc ( 'This should never happen. PHP_DEBUG_ON is on but TRACE_DEBUG_LOG is not defined. That is strange!' ), E_USER_WARNING );
		$this->message = $message;
		$this->code = $code;
		parent::__construct ( $message, $code, $previous );
	}
	public function __construct($message, $code = null, $previous = null) {
		$this->_trigger_exception ( $message, $code, $previous );
	}
}
?>