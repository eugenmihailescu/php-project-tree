<?php
require_once 'lib/MyException.php';
require_once 'GenericArchive.php';

class MyZipArchive extends GenericArchive {

	private $_zip;

	private $_errcode;
	private $_opened;

	function __construct( $filename, $provider = null ) {
		$filename = $filename . '.zip';
		parent::__construct( $filename, $provider );
		$this->_zip = new \ZipArchive();
		$this->_opened = true === $this->open( $filename );
		
		$this->setDefaultComment();
	}

	function __destruct() {
		is_resource( $this->_zip ) && $this->close();
	}

	public function open( $filename = null ) {
		$this->close();
		$filename = empty( $filename ) ? $this->getFileName() : $filename;
		
		return ( $this->_errcode = $this->_zip->open( $filename, \ZipArchive::CREATE ) );
	}

	public function close() {
		$this->_opened = ! ( $result = ! $this->_opened || ( $this->_errcode = $this->_zip->close() ) );
		return $result;
	}

	public function addFile( $filename, $name = null, $compress = true ) {
		if ( ! ( $abort_signal_received = parent::addFile( $filename, $name, $compress ) ) )
			return false;
			
			// strip the Win drive or trailing slash
		preg_match( '/^(\w:)?\\' . DIRECTORY_SEPARATOR . '(.*)/', empty( $name ) ? $filename : $name, $matches ) &&
			 $name = $matches[2];
		
		$name = str_replace( '\\', '/', $name ); // forward-slash is prefered by ZIP although it supports the backslash too
		$fsize = filesize( $filename );
		
		$this->_zip->addFile( $filename, $name );
		$this->onProgress( $filename, $fsize, $fsize, $this );
		return true;
	}

	public function compress( $method, $level ) {
		// $this->close();
		return $this->getFileName();
	}

	public function decompress( $method = null, $uncompress_size = 0 ) {
		return $this->getFileName();
	}

	public function getArchiveFiles( $filename = null ) {
		file_exists( $filename ) && $this->open( $filename );
		$result = array();
		for ( $i = 0; $i < $this->_zip->numFiles; $i++ ) {
			$stat = $this->_zip->statIndex( $i );
			$result[$stat['index']] = array( 
				'name' => $stat['name'], 
				'time' => $stat['mtime'], 
				'size' => $stat['size'], 
				'checksum' => $stat['crc'], 
				'compressed' => $stat['comp_size'], 
				'level' => $stat['comp_method'] );
		}
		return $result;
	}

	public function extract( $filename = null, $dst_path = null, $force_extrct = true ) {
		$filename = empty( $filename ) ? $this->getFileName() : $filename;
		
		if ( $result = false !== ( $zip_files = $this->getArchiveFiles( $filename ) ) )
			! ( empty( $dst_path ) || file_exists( $dst_path ) ) && $result = mkdir( $dst_path, 0770, true );
		
		if ( ! $result )
			return false;
		
		$result = array();
		$abort_signal_received = false;
		$max = count( $zip_files );
		$i = 1;
		
		$is_win = preg_match( '/^win/i', PHP_OS );
		foreach ( $zip_files as $file_header ) {
			if ( _is_callable( $this->onAbortCallback ) && ( $abort_signal_received = $abort_signal_received ||
				 false !== _call_user_func( $this->onAbortCallback/*, $this->getProvider() */) ) )
				break;
			
			if ( ! empty( $dst_path ) || ! $is_win )
				$output_file = $this->_addTrailingSlash( $dst_path );
			
			$orig_filename = $file_header['name'];
			// trim the drive part from the original filename
			! empty( $dst_path ) && $is_win && $orig_filename = preg_replace( '@\w*:[\\\/]@', '', $orig_filename );
			
			$output_file .= str_replace( array( '\\', '/' ), DIRECTORY_SEPARATOR, $orig_filename );
			
			if ( DIRECTORY_SEPARATOR == substr( $file_header['name'], - 1 ) && ! empty( $file_header['name'] ) ) {
				$this->_mk_dir( $output_file );
				$this->onProgress( $filename, $i++, $max, $this );
				continue;
			} else
				$this->_mk_dir( dirname( $output_file ) );
			
			$error = false;
			$fr = $this->_zip->getStream( $file_header['name'] );
			$fw = fopen( $output_file, 'wb' );
			if ( false !== $fr ) {
				if ( false !== $fw ) {
					if ( $error = ( $file_header['size'] != ( $bw = $this->_pipeStreams( $fr, $fw ) ) ) ) {
						$this->_stdOutput( 
							sprintf( 
								_esc( '[!] Wrote only %d of %d bytes to %s' ), 
								$bw, 
								$file_header['size'], 
								$output_file ) );
						$fsize = filesize( $filename ) / ( 1000 * 100 ); // a block of 100k
					}
					fclose( $fw );
				}
				fclose( $fr );
			}
			
			$this->onProgress( $filename, $i++, $max, $this, 0 );
			
			if ( $error && $force_extrct )
				$this->_stdOutput( 
					sprintf( 
						_esc( '[!] Extracting the file %s forcebly (its content may be truncated)' ), 
						$output_file ) );
				// we don't return files that has the smallest speck of error!
			( ! $error || $force_extrct ) && $result[$file_header['name']] = $output_file;
		}
		return $result;
	}

	public function setArchiveComment( $comment ) {
		return true === $this->_errcode && $this->_zip->setArchiveComment( $comment );
	}

	/**
	 * Requires PHP 5.6+
	 *
	 * @see GenericArchive::setArchivePassword()
	 */
	public function setArchivePassword( $password ) {
		if ( version_compare( PHP_VERSION, '5.6', '<' ) )
			return false;
		
		return true === $this->_errcode && $this->_zip->setPassword( $password );
	}

	public function unlink() {
		// in case of ZIP we do nothing
	}
}
?>