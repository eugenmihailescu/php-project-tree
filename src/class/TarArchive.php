<?php
define ( 'TAR_MAGIC', 'ustar ' );
define ( 'TAR_VERSION', '00' );
define ( 'TAR_VERSION_EX', " \0" );
define ( 'TAR_MAGIC_OFFSET', 257 );
define ( 'TAR_EXTHEADER_LEN', 512 );
define ( 'TAR_BUFFER_LENGTH', 8192 );
define ( 'TAR_LONGLINK', '././@LongLink' );
define ( 'BZ_OK', 0 );

require_once  'lib/MyException.php';
require_once 'GenericArchive.php';

class TarArchive extends GenericArchive {

	function __construct($filename, $provider = null) {
		parent::__construct ( $filename . '.tar', $provider );
		$this->setDefaultComment();
	}

	private function _isValidHeader($header) {
		$tar_maginc_len = strlen ( TAR_MAGIC );
		$ext_header = TAR_LONGLINK == substr ( $header, 0, strlen ( TAR_LONGLINK ) );
		$offset = $ext_header ? 2 * TAR_EXTHEADER_LEN : 0;
		
		$tar_magic_ok = TAR_MAGIC == substr ( $header, $offset + TAR_MAGIC_OFFSET, $tar_maginc_len );
		$tar_version_ok = TAR_VERSION ==
			 substr( $header, $offset + TAR_MAGIC_OFFSET + $tar_maginc_len, strlen( TAR_VERSION ) );
		$tar_version_ext_ok = TAR_VERSION_EX ==
			 substr( $header, $offset + TAR_MAGIC_OFFSET + $tar_maginc_len, strlen( TAR_VERSION_EX ) );
		
		return $tar_magic_ok && ($tar_version_ok || $tar_version_ext_ok);
	}

	/**
	 * Returns the TAR POSIX 1 header for a file
	 *
	 * @see /usr/include/tar.h
	 *
	 * @param string $filename        	
	 * @param string $name        	
	 * @return string
	 */
	private function _getFileHeader($filename, $name = null) {
		/* http://www.mkssoftware.com/docs/man4/tar.4.asp */
		/* http://www.gnu.org/software/tar/manual/html_node/Standard.html */
		/* read more about LongLink: http://www.delorie.com/gnu/docs/tar/tar_114.html */
	
		$finfo = stat ( $filename );
		$falias = null == $name ? $filename : $name;
		
		// remove trailing slash
		if (strlen ( $falias ) > 0 && DIRECTORY_SEPARATOR == substr ( $falias, 0, 1 ))
			$falias = substr ( $falias, 1 );
		
		if (strlen ( $falias ) > 512)
			return false;
		preg_match( '/^(\w:)?\\' . DIRECTORY_SEPARATOR . '(.*)/', $falias, $matches ) && $falias = $matches[2];
		
		$falias = str_replace ( '\\', '/', $falias ); // forward-slash is prefered by tar although it supports the backslash too
		$bigheader = $header = '';
		
		if ( function_exists( '\\posix_getpwuid' ) ) {
			$posix_getpwuid = posix_getpwuid ( $finfo ['uid'] );
			$posix_getpwuid = posix_getpwuid ( $finfo ['gid'] );
			
			$user_name = $posix_getpwuid ["name"];
			$group_name = $posix_getpwuid ["name"];
		} else {
			$user_name = getenv ( 'USERNAME' );
			$group_name = $user_name;
		}
		
		// analyzing a .tar header (create a dummy tar, rename its extension with .o and view it with a hexdump/mcview) it seems that its structure is as following:
		// if len(filename)>100 then:
		// - the first block must be a bigheader block as bellow followed by a 512 block containing only the filename (padded by NULs)
		// - the second block is a normal header block as describe later below
		if (strlen ( $falias ) > 100) {
			// 1st 512-bytes block
			$bigheader = pack( 
				"a100a8a8a8a12a12a8a1a100a6a2a32a32a8a8a155a12", 
				TAR_LONGLINK, 
				sprintf( '%08o', $finfo['mode'] ), 
				'0000000', 
				'0000000', 
				sprintf( "%011o", strlen( $falias ) ), 
				'00000000000', 
				str_repeat( ' ', 8 )/*chksum*/, 'L', 
				'', 
				TAR_MAGIC, 
				TAR_VERSION, 
				$user_name, 
				$group_name, 
				'', 
				'', 
				'', 
				'' );
			
			// 2nd 512-bytes block (contains only the filename)
			// $bigheader .= str_pad ( $falias, floor ( (strlen ( $falias ) + TAR_EXTHEADER_LEN - 1) / TAR_EXTHEADER_LEN ) * TAR_EXTHEADER_LEN, "\0" );
			$bigheader .= pack ( "a512", $falias );
			
			// calculate the block's cummulated checksum
			$checksum = 0;
			for($i = 0; $i < TAR_EXTHEADER_LEN; $i ++)
				$checksum += ord ( substr ( $bigheader, $i, 1 ) );
			$bigheader = substr_replace ( $bigheader, sprintf ( "%06o", $checksum ) . "\0 ", 148, 8 );
		}
		switch (filetype ( $filename )) {
			case 'dir' :
				$typeflag = 5;
				break;
			case 'link' :
				$typeflag = 1;
				break;
			case 'fifo' :
				$typeflag = 6;
				break;
			case 'char' :
				$typeflag = 3;
				break;
			case 'block' :
				$typeflag = 4;
				break;
			default :
				$typeflag = 0; // ie. regular file
				break;
		}
		$header = pack ( "a100a8a8a8a12a12a8a1a100a6a2a32a32a8a8a155a12", 		// 512 bytes
		substr ( $falias, 0, 100 ), 		// file alias/name
		sprintf ( '%08o', $finfo ['mode'] ), 		// file mode (see http://php.net/manual/en/function.stat.php#34919)
		sprintf ( "%08o", $finfo ['uid'] ), 		// user id
		sprintf ( "%08o", $finfo ['gid'] ), 		// group id
		sprintf ( "%012o", $finfo ['size'] ), 		// file size
		sprintf ( "%012o", $finfo ['mtime'] ), 		// modified time
		str_repeat ( ' ', 8 ), 		// checksum (initially must be 8 blanks)
		$typeflag, 		// is a tranlation of `mode` (see http://www.gnu.org/software/tar/manual/html_node/Standard.html)
		is_link ( $filename ) ? readlink ( $filename ) : '', 		// if $filename is a link then this is the link target
		TAR_MAGIC, 		// the magic signature
		TAR_VERSION, 		// the version id
		$user_name, 		// user name
		$group_name, 		// group name
		'', 		// in case of BLKTYPE devices this represents the block major number
		'', 		// in case of BLKTYPE devices this represents the block minor number
		substr ( $falias, 100, 155 ), 		// prefix used to fill any filename chars above offset 100 to 155 (thus len=155)
		'' );
		
		// calc & update the header checksum
		$checksum = 0;
		for($i = 0; $i < TAR_EXTHEADER_LEN; $i ++)
			$checksum += ord ( substr ( $header, $i, 1 ) );
		$header = substr_replace ( $header, sprintf ( "%06o", $checksum ) . "\0 ", 148, 8 );
		
		return $bigheader . $header; // 512-1536 bytes long (1-3 x 512-bytes blocks)
	}

	private function _extractHeader($buffer) {
		$blen = strlen ( $buffer );
		if ($blen < TAR_EXTHEADER_LEN)
			return false;
		
		$trim = function ($var) {
			return rtrim ( ltrim ( str_replace ( "\0", '', $var ) ) );
		};
		
		if (! $this->_isValidHeader ( $buffer ))
			return false;
		
		if ($ext_header = TAR_LONGLINK == substr ( $buffer, 0, strlen ( TAR_LONGLINK ) )) {
			$fname = substr ( $buffer, TAR_EXTHEADER_LEN, TAR_EXTHEADER_LEN );
			$buffer = substr ( $buffer, 2 * TAR_EXTHEADER_LEN );
		} else
			$fname = substr ( $buffer, 0, 100 );
		
		$fmode = substr ( $buffer, 100, 8 );
		$uid = substr ( $buffer, 108, 8 );
		$gid = substr ( $buffer, 116, 8 );
		$fsize = substr ( $buffer, 124, 12 );
		$mtime = substr ( $buffer, 136, 12 );
		$chksum = substr ( $buffer, 148, 8 );
		$is_link = substr ( $buffer, 156, 1 );
		$linkname = substr ( $buffer, 157, 100 );
		
		$result = array (
				'mode' => octdec ( $fmode ),
				'uid' => octdec ( $uid ),
				'gid' => octdec ( $gid ),
				'size' => octdec ( $fsize ),
				'time' => octdec ( $mtime ),
				'chksum' => octdec ( $chksum ),
				'link' => $is_link,
			'linkname' => $trim( $linkname ) );
		
		$prefix = '';
		
		if ($ext_header) {
			$uname = substr ( $buffer, 265, 32 );
			$gname = substr ( $buffer, 297, 32 );
			$dev_major = substr ( $buffer, 329, 8 );
			$dev_minor = substr ( $buffer, 337, 8 );
			// $prefix = substr ( $buffer, 345, 155 );
			
			$result += array (
					'uname' => $trim ( $uname ),
					'gname' => $trim ( $gname ),
					'major' => $trim ( $dev_major ),
				'minor' => $trim( $dev_minor ) );
		}
		
		$result ['name'] = $trim ( $fname );
		
		return $result;
	}

	/**
	 * Add a file to the TAR archive
	 *
	 * @param string $filename        	
	 * @param string $name        	
	 */
	public function addFile( $filename, $name = null, $compress = true ) {
		if ( $abort_signal_received = ! parent::addFile( $filename, $name, $compress ) )
			return false;
		
		$fsize = filesize ( $filename );
		
		$err = false;
		if (false === ($header = $this->_getFileHeader ( $filename, $name )))
			return false;
		
		$fw = fopen ( $this->getFileName (), 'ab' );
		if (false !== $fw)
			fwrite ( $fw, $header );
		else
			throw new MyException ( sprintf ( _esc ( 'Cannot open file %s in write-mode' ), $this->getFileName () ) );
		
		try {
			$chunksize = MB; // 1Mb buffer
			if ($fsize > $chunksize) {
				$bw = 0;
				$fr = fopen ( $filename, 'rb' );
				
				// NOTE: flock does not work for FAT partition
				if (flock ( $fr, LOCK_EX )) {
					while ( ! feof ( $fr ) && false === $abort_signal_received ) {
						if ( _is_callable( $this->onAbortCallback ) && ( $abort_signal_received = $abort_signal_received ||
							 false !== _call_user_func( $this->onAbortCallback/*, $this->getProvider()*/ ) ) )
							break;
						
						$bw += fwrite ( $fw, fread ( $fr, $chunksize ) );
						$this->onProgress ( $filename, $bw, $fsize, $this );
					}
					flock ( $fr, LOCK_UN );
				} else {
					$this->_stdOutput ( _esc ( 'Could not gain exclusive access for ' ) . $filename );
					
					fseek ( $fw, - strlen ( $header ), SEEK_END ); // ignore the last header
				}
				fclose ( $fr );
			} else {
				fwrite ( $fw, file_get_contents ( $filename ) );
				$this->onProgress ( $filename, $fsize, $fsize, $this );
			}
			// Pad file contents to byte count divisible by TAR_EXTHEADER_LEN
			$pad_len = ceil ( ($fsize) / TAR_EXTHEADER_LEN ) * TAR_EXTHEADER_LEN - $fsize;
			$pad_len > 0 && fwrite ( $fw, str_repeat ( chr ( 0 ), $pad_len ) );
		} catch ( Exception $err ) {
		}
		
		fclose ( $fw );
		
		if (false !== $err)
			throw new MyException ( $err->getMessage (), $err->getCode (), $err->getPrevious () );
		
		return false === $abort_signal_received;
	}

	/**
	 * Compress the TAR archive
	 *
	 * @param int $method        	
	 * @param int $level        	
	 * @return string
	 */
	public function compress($method, $level) {
		if (NONE == $method)
			return $this->getFileName ();
		
		global $COMPRESSION_NAMES;
		$abort_signal_received = false;
		$ext = '.' . $COMPRESSION_NAMES [$method];
		$filter = '';
		
		list ( $filter, $mode ) = $this->_getFilterMode ( $method, $level, false );
		
		if (! _function_exists ( $filter . 'open' ))
			throw new MyException( 
				sprintf( 
					_esc( 
						'%s support is not enabled. Check your PHP configuration (php.ini) or contact your hosting provider.' ), 
					strtoupper( $filter ) ) );
		
		$output_file = $this->getFileName () . $ext;
		file_exists ( $output_file ) && @unlink ( $output_file ); // overwrite the existent compressed archive
		
		if (! file_exists ( $this->getFileName () ))
			throw new MyException( 
				sprintf( _esc( "Cannot compress the file %s due to it does not exist" ), $this->getFileName() ) );
		
		if ('' != $filter) {
			$fsize = filesize ( $this->getFileName () );
			$fw = _call_user_func ( $filter . 'open', $output_file, $mode );
			
			$fr = fopen ( $this->getFileName (), 'rb' );
			if (false !== $fr) {
				$bw = 0;
				while ( ! feof ( $fr ) && false === $abort_signal_received ) {
					if ( _is_callable( $this->onAbortCallback ) && ( $abort_signal_received = $abort_signal_received ||
						 false !== _call_user_func( $this->onAbortCallback/*, $this->getProvider() */) ) )
						break;
					
					$bw += _call_user_func ( $filter . 'write', $fw, fread ( $fr, MB ) );
					
					if (_is_callable ( $this->onProgressCallback ))
						_call_user_func( 
							$this->onProgressCallback, 
							$this->getProvider(), 
							$this->getFileName(), 
							$bw, 
							$fsize, 
							3 );
					
					$this->getCPUSleep () > 0 && _usleep ( 1000 * $this->getCPUSleep () );
				}
				
				fclose ( $fr );
			}
			_call_user_func ( $filter . 'close', $fw );
		}
		return false === $abort_signal_received ? $this->getFileName () . $ext : false;
	}

	/**
	 * Uncompress the compressed TAR archive
	 *
	 * @param int $method The TAR archive compression method identifier; specify null to autodetect (by extension)
	 * @param int $uncompress_size The expected size of the uncompressed files, 0 when is unknown
	 * @param string $new_name The uncompressed file's name. When not specified is calculated automatically by cutting the $method's associated extension
	 * @throws MyException
	 * @return Returns the TAR filename resulted after decompression
	 */
	public function decompress($method = null, $uncompress_size = 0, $new_name = null) {
		global $COMPRESSION_NAMES;
		$gz_uncompressed = function ($filename) {
			$result = 0;
			$fr = fopen ( $filename, 'rb' );
			if (false !== $fr) {
				fseek ( $fr, - 4, SEEK_END );
				$buff = fread ( $fr, 4 );
				$array = unpack ( 'V', $buff );
				$result = end ( $array );
				fclose ( $fr );
			}
			return $result;
		};
		
		$result = false;
		$abort_signal_received = false;
		
		if ( null === $method &&
			 preg_match( '/\.((' . implode( '|', $COMPRESSION_NAMES ) . ')$)/i', $this->getFileName(), $matches ) ) {
			// preg_match ( '/\D*/', $matches [2], $matches ) &&
			($filter = $matches [2]) && $method = array_search ( $filter, $COMPRESSION_NAMES );
			$mode = 'r';
		}
		
		(null !== $method) && list ( $filter, $mode ) = $this->_getFilterMode ( $method );
		
		if (empty ( $filter ) || ! _function_exists ( $filter . 'open' ))
			throw new MyException( 
				sprintf( 
					_esc( 
						'%s support is not enabled. Check your PHP configuration (php.ini) or contact your hosting provider.' ), 
					strtoupper( $filter ) ) );
		
		if (! empty ( $filter )) {
			if (! $this->fixArchiveCRLF ( $this->getFileName (), $method )) // try to detect & fix bad archives; otherwhise just throw an exception
				throw new MyException( 
					sprintf( _esc( 'Archive %s has a bad signature. Unsupported format.' ), $this->getFileName() ) );
			
			0 == $uncompress_size && GZ == $method && $uncompress_size = $gz_uncompressed ( $this->getFileName () );
			
			if (! empty ( $new_name ))
				$result = $new_name;
			else
				$result = preg_replace( 
					'/(\.(' . implode( '|', $COMPRESSION_NAMES ) . '))$/i', 
					'', 
					$this->getFileName() );
			
			$fr = _call_user_func ( $filter . 'open', $this->getFileName (), $mode );
			$eof_func = (GZ == $method ? 'gz' : 'f') . 'eof';
			$fw = fopen ( $result, 'wb' );
			
			$error = false;
			$bw = 0;
			
			if (false !== $fr) {
				$uncompress_size = 0; // FIXME this overrides the value calculated above; why?
				                      
				// FIXME: a bz2 archive which compressed externally may return EOF when bzread reached 100K * compression-level bytes (eg. for -9 it decompress only the first 900K
				                      // then it report EOF)
				                      // read more: http://www.bzip.org/1.0.5/bzip2-manual-1.0.5.html#memory-management
				while ( ! _call_user_func ( $eof_func, $fr ) && false === $abort_signal_received ) {
					if ( _is_callable( $this->onAbortCallback ) && ( $abort_signal_received = $abort_signal_received ||
						 false !== _call_user_func( $this->onAbortCallback/*, $this->getProvider() */) ) )
						break;
					
					$str = _call_user_func ( $filter . 'read', $fr, TAR_BUFFER_LENGTH );
					$bw += strlen ( $str );
					
					$uncompress_size < 0 && $bw > $uncompress_size && $bw = $uncompress_size; // FIXME $uncompress_size<0 ??? does this line execute at least once?
					
					if (false === $str)
						$error = sprintf ( _esc ( '%s problem' ), $filter );
					elseif (BZ2 == $method && BZ_OK !== bzerrno ( $fr ))
						$error = bzerrstr ( $fr );
					
					if (false !== $error)
						break;
					
					fwrite ( $fw, $str );
					
					_is_callable( $this->onProgressCallback ) && _call_user_func( 
						$this->onProgressCallback, 
						$this->getProvider(), 
						$this->getFileName(), 
						$bw, 
						$uncompress_size, 
						8 );
					
					$this->getCPUSleep () > 0 && _usleep ( 1000 * $this->getCPUSleep () );
				}
				_is_callable( $this->onProgressCallback ) && _call_user_func( 
					$this->onProgressCallback, 
					$this->getProvider(), 
					$this->getFileName(), 
					$uncompress_size, 
					$uncompress_size, 
					8 ); // force
						     // it
						     // to
						     // 100%
			}
			
			fclose ( $fw );
			_call_user_func ( $filter . 'close', $fr );
			
			if ($error)
				throw new MyException ( $error );
			
			$bw < $uncompress_size && $this->_stdOutput( 
				sprintf( _esc( '[!] Expected %d bytes to decompress but I got only %d' ), $uncompress_size, $bw ) );
		}
		
		return $result;
	}

	/**
	 * Extract the TAR archive files header (not the file content)
	 *
	 * @param string $filename The TAR file name
	 * @return Return false if invalid archive otherwise an array containing an record for each file within TAR
	 */
	public function getArchiveFiles($filename = null) {
		$filename = empty ( $filename ) ? $this->getFileName () : $filename;
		$max_offset = filesize ( $filename );
		if (! ($result = file_exists ( $filename ) && $this->isValidTar ( $filename )))
			return false;
		
		$result = array ();
		if (! ($fr = fopen ( $filename, 'rb' )))
			return false;
		
		$offset = 0;
		while ( $offset < $max_offset && - 1 != fseek ( $fr, $offset, SEEK_SET ) ) {
			
			$buffer = fread ( $fr, TAR_BUFFER_LENGTH );
			
			if (false === $buffer)
				continue;
				
				// detect & handle corrupted TAR headers
			if (false === ($array = $this->_extractHeader ( $buffer ))) {
				$found = false;
				
				$this->_stdOutput( 
					sprintf( 
						_esc( '[!] Invalid TAR header at offset %d %sSkipping to the next header...' ), 
						$offset, 
						count( $result ) > 0 ? sprintf( 
							_esc( 'Probably the file %s has a truncated content.' ), 
							$result[count( $result ) - 1]['name'] ) . PHP_EOL : '.' ) );
				
				$old_offset = $offset;
				if (false !== ($p = strpos ( $buffer, TAR_MAGIC . TAR_VERSION ))) {
					$offset += $p - TAR_MAGIC_OFFSET;
					$found = true;
				} else
					while ( $offset < $max_offset && ! feof ( $fr ) ) {
						if ( false !== ( $buffer = fread( $fr, TAR_BUFFER_LENGTH ) ) &&
							 false !== ( $p = strpos( $buffer, TAR_MAGIC . TAR_VERSION ) ) ) {
							$offset = ftell ( $fr ) - strlen ( $buffer );
							$found = true;
							break; // fount another header in the subsequent data blocks
						}
					}
				
				$this->_stdOutput( 
					$found ? sprintf( 
						_esc( 'Found the next header at offset %d (%d bytes away)' ), 
						$offset, 
						$offset - $old_offset ) : _esc( 'No other header found. I give up....' ) );
				
				if ($found)
					continue;
				else
					break;
			}
			
			$fsize = ceil ( $array ['size'] / TAR_EXTHEADER_LEN ) * TAR_EXTHEADER_LEN;
			$pos = (count ( $array ) > 9 ? 3 : 1) * TAR_EXTHEADER_LEN;
			
			$array ['offset'] = $offset + $pos;
			$result [] = $array;
			$offset += $pos + $fsize; // move to next header
		}
		fclose ( $fr );
		return $result;
	}

	/**
	 * Extracts the files from TAR archive
	 *
	 * @param string $filename The tar archive filename
	 * @param string $dst_path The destination path where the files within TAR archive are extracted
	 * @param bool $force_extrct Extract forcebly the file even if it has a truncated content or some other problem
	 * @return Returns false if invalid archive otherwise returns an array where the key=source filename and the value=temporary extracted filename
	 */
	public function extract($filename = null, $dst_path = null, $force_extrct = true) {
		// $progress_step = function ($filename, $i, $max, &$obj) {
		// if (_is_callable ( $obj->onProgressCallback ))
		// _call_user_func ( $obj->onProgressCallback, $obj->provider, $filename, $i, $max, 3 );
		// };
		$filename = empty ( $filename ) ? $this->getFileName () : $filename;
		
		if ($result = false !== ($tar_files = $this->getArchiveFiles ( $filename )))
			! (empty ( $dst_path ) || file_exists ( $dst_path )) && $result = mkdir ( $dst_path, 0770, true );
		
		if (! $result)
			return false;
		
		if (! ($fr = fopen ( $filename, 'rb' )))
			return false;
		
		$result = array ();
		$abort_signal_received = false;
		$max = count ( $tar_files );
		$i = 1;
		
		$is_win = preg_match ( '/^win/i', PHP_OS );
		foreach ( $tar_files as $file_header ) {
			if ( _is_callable( $this->onAbortCallback ) && ( $abort_signal_received = $abort_signal_received ||
				 false !== _call_user_func( $this->onAbortCallback/*, $this->getProvider() */) ) )
				break;
			
			if (! empty ( $dst_path ) || ! $is_win)
				$output_file = $this->_addTrailingSlash ( $dst_path );
			
			$orig_filename = $file_header ['name'];
			! empty ( $dst_path ) && $is_win && $orig_filename = preg_replace ( '@\w*:[\\\/]@', '', $orig_filename ); // trim the drive part from the original filename
			
			$output_file .= str_replace( array( '\\', '/' ), DIRECTORY_SEPARATOR, $orig_filename );
			
			// if (DIRECTORY_SEPARATOR == substr ( $file_header ['name'], - 1 ) && ! empty ( $file_header ['name'] )) {
			// 16384 = 0040000 octal = `directory` mode (see http://php.net/manual/en/function.stat.php#34919)
			if (($file_header ['mode'] & 16384) && ! empty ( $file_header ['name'] )) {
				$this->_mk_dir ( $output_file );
				$this->onProgress ( $filename, $i ++, $max, $this, 0 );
				continue;
			} else {
				$this->_mk_dir ( dirname ( $output_file ) );
			}
			
			$error = false;
			$fw = fopen ( $output_file, 'wb' );
			if (false !== $fw) {
				if ( $error = ( $file_header['size'] !=
					 ( $bw = $this->_pipeStreams( $fr, $fw, $file_header['size'], $file_header['offset'] ) ) ) ) {
					$this->_stdOutput( 
						sprintf( 
							_esc( '[!] Wrote only %d of %d bytes to %s' ), 
							$bw, 
							$file_header['size'], 
							$output_file ) );
					$fsize = filesize ( $filename ) / (1000 * 100); // a block of 100k
					$fsize > 0 && $fsize < 10 && $this->_stdOutput( 
						sprintf( 
							_esc( 
								'The TAR archive has odd filesize of %d*100k. Was it initially compressed with PBzip2 with -%d flag?\nIf that is the case then the %s is probably truncated thus unreliable.' ), 
							$fsize, 
							$fsize, 
							basename( $filename ) ) );
				}
				
				fclose ( $fw );
			}
			
			$this->onProgress ( $filename, $i ++, $max, $this, 0 );
			
			if ($error && $force_extrct)
				$this->_stdOutput( 
					sprintf( 
						_esc( '[!] Extracting the file %s forcebly (its content may be truncated)' ), 
						$output_file ) );
			(! $error || $force_extrct) && $result [$file_header ['name']] = $output_file; // we don't return files that has the smallest speck of error!
		}
		fclose ( $fr );
		return $result;
	}

	/**
	 * Check whether the specified $filename is a valid TAR file
	 *
	 * @param string $filename        	
	 * @return boolean Returns true if valid TAR archive, false otherwise.
	 */
	public function isValidTar($filename) {
		if (! file_exists ( $filename ))
			return false;
		$result = false;
		$fr = fopen ( $filename, 'rb' );
		if (false !== $fr) {
			$header = fread ( $fr, 3 * TAR_EXTHEADER_LEN );
			$result = $this->_isValidHeader ( $header );
			fclose ( $fr );
		}
		return $result;
	}
	
	/*
	 * (non-PHPdoc) @see GenericArchive::isValidArchive()
	 */
	public function isValidArchive( $filename, $method = null ) {
		if ( NONE == $method )
			return $this->isValidTar( $filename );
		else
			return parent::isValidArchive( $filename, $method );
	}
}
?>