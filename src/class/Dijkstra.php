<?php
/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implements the Dijkstra algorithm for finding the shortest path between two nodes in a generic graph
 * It also helps you defining the graph as well as extracting some aggregated data
 *
 * @author Eugen Mihailescu
 * @version 0.1
 * @since 2015-04-10
 *       
 */
require_once 'GenericGraph.php';
class Dijkstra extends GenericGraph {
	/**
	 * Moves a node $node from the $w|$p array to the $dest array
	 */
	function _move(&$w, &$p, &$dest, $node) {
		$dest [$node] = array (
				'w' => $w [$node],
				'p' => $p [$node] 
		);
		unset ( $w [$node] );
		unset ( $p [$node] );
	}
	/**
	 * Traverse the graph and finds the shortest path and the cost between a starting node and any other node of the graph
	 *
	 * @param string $i
	 *        	The initial/starting node of the graph
	 * @param bool $real_name
	 *        	False when $i node's name is from the internal lookup table, true otherwise
	 * @return array Returns an array where each item contains info about the shortest path between $i and the respective node.
	 *         The key is the item's lookup node node, the value contains the weight and parent node.
	 */
	public function traverse($i, $real_name = false) {
		// use the integer lookup value instead of its original name
		$real_name && isset ( $this->_lookup [$i] ) && $i = $this->_lookup [$i];
		
		$result = array ();
		$w = array ();
		$p = array ();
		
		// graph traversal initialization
		foreach ( $this->_nodes as $v ) {
			$w [$v] = $v == $i ? 0 : $this->getEdgeLen ( $i, $v );
			$p [$v] = $v == $i ? null : $i; // the parent of the node $n (null for itself)
		}
		
		isset ( $w [$i] ) && $this->_move ( $w, $p, $result, $i ); // move the initial node in result
		
		while ( ! empty ( $w ) ) {
			if (($u = array_keys ( $w, min ( $w ) )) == $this->_infinity)
				break;
			
			$u = &$u [0];
			
			foreach ( $this->getAdiacentNodes ( $u ) as $v => $l )
				if (isset ( $w [$v] )) {
					$d = $w [$u] + $l;
					if ($d < $w [$v]) {
						$w [$v] = $d;
						$p [$v] = $u;
					}
				}
			$this->_move ( $w, $p, $result, $u ); // move the traversed node in result
		}
		
		return $result;
	}
	
	/**
	 * Returns the shortest path between an initial node $from and a final node $to of the graph
	 *
	 * @param string $from        	
	 * @param string $to        	
	 * @param bool $real_name
	 *        	When true then $from,$to are assumed to be node real name otherwise they are just lookups ids
	 * @return Returns an array where the key represent a node of the path and the value the weight from that node to the next one, false if no route
	 */
	public function getShortPath($from, $to) {
		$bak = $to;
		// use the integer lookup value instead of their original name
		
		$from = isset ( $this->_lookup [$from] ) ? $this->_lookup [$from] : false;
		$to = isset ( $this->_lookup [$to] ) ? $this->_lookup [$to] : false;
		
		if (false !== $from && false !== $to) {
			$t = $this->traverse ( $from );
			$result = array (
					$bak => $t [$to] ['w'] 
			);
			while ( ($to = $t [$to] ['p']) != null )
				$result [array_search ( $to, $this->_lookup )] = $t [$to] ['w'];
			return array_reverse ( $result );
		}
		return false;
	}
}
?>