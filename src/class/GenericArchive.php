<?php
require_once 'lib/MyException.php';
abstract class GenericArchive {
	private $archive_files;
	private $fileName;
	private $options;
	public $provider;
	public $cpusleep;
	public $onAbortCallback;
	public $onProgressCallback;
	public $onStdOutput;
	
	// \\\\\\\\\\\\\\\\\ PROTECTED //////////////////
	protected function _getFilterMode($method, $level = 0, $read = true) {
		global $COMPRESSION_FILTERS;

		if ( isset( $COMPRESSION_FILTERS[$method] ) && count( $COMPRESSION_FILTERS[$method] ) > 1 ) {
		$filter = $COMPRESSION_FILTERS [$method] [0];
			$mode = $COMPRESSION_FILTERS[$method][1];
		} else {
			$filter = '';
			$mode = '';
		}
		
		$mode = $read ? 'r' . ( GZ == $method ? 'b' : '' ) : sprintf( $mode, $level );
		
		return array( $filter, $mode );
	}
	protected function _stdOutput($str) {
		// wrap lines that ends in \n | \n\r
		foreach ( explode ( '\n', str_replace ( PHP_EOL, '\n', $str ) ) as $str )
			if (_is_callable ( $this->onStdOutput ))
				_call_user_func ( $this->onStdOutput, "<yellow>$str</yellow>" );
			else
				echo $str, PHP_EOL;
	}
	protected function _addTrailingSlash($path) {
		return $path .
			 ( substr( $path, - strlen( DIRECTORY_SEPARATOR ) ) != DIRECTORY_SEPARATOR ? DIRECTORY_SEPARATOR : '' );
	}
	protected function _mk_dir($path, $dir_sep = DIRECTORY_SEPARATOR) {
		return file_exists ( $path ) || mkdir ( $path, 0770, true );
	}

	/**
	 * Copy $maxlength bytes from $in stream to $out stream starting at position $offset
	 *
	 * @param stream $in        	
	 * @param stream $out        	
	 * @param int $maxlength        	
	 * @param int $offset        	
	 * @return int Returns the number of bytes copied, 0 otherwise
	 * @see http://php.net/manual/en/function.stream-copy-to-stream.php
	 */
	protected function _pipeStreams($in, $out, $maxlength = -1, $offset = -1) {
		$size = 0;
		( - 1 != $offset ) && ( 0 == fseek( $in, $offset, SEEK_SET ) ) || $offset < 0 || $maxlength = 0; // !seek =>
		                                                                                                 // return
		
		while ( ! feof ( $in ) && (- 1 == $maxlength || $size + TAR_BUFFER_LENGTH < $maxlength) ) {
			// false !== ($buff = fread ( $in, TAR_BUFFER_LENGTH )) && $size += fwrite ( $out, $buff );
			if (false !== ($buff = fread ( $in, TAR_BUFFER_LENGTH )))
				$size += fwrite ( $out, $buff );
		}
		- 1 != $maxlength && $maxlength - $size > 0 && $size += fwrite ( $out, fread ( $in, $maxlength - $size ) );
		return $size;
	}
	protected function onProgress($filename, $bw, $fsize, &$obj, $threshold = MB) {
		// we don't stress the progress for files smaller than 1MB
		$fsize > $threshold && _is_callable( $obj->onProgressCallback ) &&
			 _call_user_func( $obj->onProgressCallback, $obj->provider, $filename, $bw, $fsize, 4 );
		($cpu_sleep = $obj->getCPUSleep ()) > 0 && _usleep ( 1000 * $cpu_sleep );
	}
	function __construct($filename, $provider = null) {
		$this->skip_empty_files = false;
		$this->fileName = $filename;
		$this->provider = $provider;
		$this->cpusleep = 0;
		null == $provider && $this->provider = - 2;
		$this->onAbortCallback = null;
		$this->onProgressCallback = null;
		$this->options = array( 'skip_empty_files' => $this->skip_empty_files );
		$this->archive_files = array();
	}
	
	// \\\\\\\\\\\\\\\\\ ABSTRACT //////////////////
	
	/**
	 * Add a file to the archive
	 *
	 * @param string $filename The file to add to archive
	 * @param string $name The file path as stored in the archive (when empty then $filename)
	 * @param bool $compress When true compress file, otherwise don't (supported by PclZip only)
	 */
	public function addFile( $filename, $name = null, $compress = true ) {
		if (! file_exists ( $filename ))
			throw new MyException ( sprintf ( _esc ( 'File %s does not exist.' ), $filename ) );
		
		$fsize = filesize ( $filename );
		$options = $this->getOptions( 'skip_empty_files' );
		
		if ( $fsize == 0 && strToBool( $this->getOptions( 'skip_empty_files' ) ) )
			return false;
		$this->archive_files[$filename] = array( $name, $compress, $fsize );
		
		return true;
	}
	/**
	 * Compress the archive
	 *
	 * @param int $method        	
	 * @param int $level        	
	 * @return string
	 */
	abstract public function compress($method, $level);
	/**
	 * Uncompress the compressed archive
	 *
	 * @param int $method The archive compression method identifier; specify null to autodetect (by extension)
	 * @throws MyException
	 * @return Returns the archive filename resulted after decompression
	 */
	abstract public function decompress($method = null, $uncompress_size = 0);
	/**
	 * Extract the archive files header (not the file content)
	 *
	 * @param string $filename The file name
	 * @return Return false if invalid archive otherwise an array containing an record for each file within archive
	 */
	abstract public function getArchiveFiles($filename = null);
	/**
	 * Extracts the files from archive
	 *
	 * @param string $filename The tar archive filename
	 * @param string $dst_path The destination path where the files within archive are extracted
	 * @return Returns false if invalid archive otherwise returns an array where the key=source filename and the
	 *         value=temporary extracted filename
	 */
	abstract public function extract($filename = null, $dst_path = null, $force_extrct = true);
	
	// \\\\\\\\\\\\\\\\\ PROTECTED //////////////////
	/**
	 * Returns the option array or just the option specified by $name.
	 *
	 * @param string $name The name of option to retrieve. If null or not supplied then the returned value is the whole
	 *        option array.
	 * @return Ambigous <boolean, array>
	 */
	protected function getOptions($name = null) {
		return empty( $name ) ? $this->options : ( isset( $this->options ) && isset( $this->options[$name] ) ? $this->options[$name] : false );
	}
	/**
	 * Returns the provider that created this object
	 *
	 * @return int Returns the value of the provider
	 */
	protected function getProvider() {
		return $this->provider;
	}
	protected function getCPUSleep() {
		return $this->cpusleep;
	}
	
	// \\\\\\\\\\\\\\\\\ PUBLIC //////////////////
	
	/**
	 * Set an array of options:
	 *
	 * "skip_empty_files"=>bool,
	 * "skip_empty_dirs"=>bool
	 *
	 * @param array $options        	
	 */
	public function setOptions($options) {
		$this->options = $options;
	}
	/**
	 * Set the number of miliseconds to sleep the CPU while compression/decompression
	 *
	 * @param int $ms        	
	 */
	public function setCPUSleep($ms) {
		$this->cpusleep = $ms;
	}
	/**
	 * Set the library in buffering mode
	 *
	 * @param string $filename        	
	 */
	public function setFileName($filename = null) {
		$this->fileName = $filename;
	}
	/**
	 *
	 * @return string Returns the archive filename
	 */
	public function getFileName() {
		return $this->fileName;
	}
	/**
	 * Returns the archive filesize
	 *
	 * @return number
	 */
	public function getFileSize() {
		if (file_exists ( $this->fileName ))
			return filesize ( $this->fileName );
		else
			return 0;
	}
	/**
	 * Get the archive uncompressed size
	 *
	 * @return number
	 */
	public function getUncompressedSize() {
		$fsize = 0;
		foreach ( $this->archive_files as $filename => $stat )
			$fsize += $stat[2];
		return $fsize;
	}

	/**
	 * Get the archive added file count
	 *
	 * @return number
	 */
	public function getFileCount() {
		return count( $this->archive_files );
	}

	/**
	 * Removes the archive from disk
	 */
	public function unlink() {
		$this->close();
		@unlink ( $this->fileName );
		$this->archive_files = array();
	}
	/**
	 * Check whether the specified $filename is a valid archive file
	 *
	 * @param string $filename        	
	 * @method int The archive compression method identifier; specify null to autodetect (by extension)
	 * @return boolean Returns true if valid archive, false otherwise.
	 */
	public function isValidArchive($filename, $method = null) {
		
		global $COMPRESSION_HEADERS, $COMPRESSION_NAMES;
		$result = true;
		
		if ( null === $method &&
			 preg_match( '/\.((' . implode( '|', $COMPRESSION_NAMES ) . ')$)/i', $filename, $matches ) )
			preg_match ( '/\D*/', $matches [2], $matches ) && $filter = $matches [0];
		
		null !== $method && list ( $filter, $mode ) = $this->_getFilterMode ( $method );
		
		if ( empty( $filter ) )
			throw new MyException ( sprintf ( _esc ( 'Could not determine the archive type for %s.' ), $filename ) );
		
		if ( in_array( $method, array( 1, 2 ) ) && ! function_exists( $filter . 'open' ) )
			throw new MyException( 
				sprintf( 
					_esc( '%s support is not enabled. Check your PHP configuration (php.ini).' ), 
					strtoupper( $filter ) ) );
		
		if (! empty ( $method )) {
			
			$hdr_len = 0;
			if (isset ( $COMPRESSION_HEADERS [$method] )) {
				$hdr_len = $COMPRESSION_HEADERS [$method] [0];
				$hdr_pattern = $COMPRESSION_HEADERS [$method] [1];
			}
			
			if (file_exists ( $filename ) && $hdr_len > 0 && false !== ($fr = fopen ( $filename, 'rb' ))) {
				$buff = fread ( $fr, $hdr_len );
				
				$result = false !== $buff && preg_match ( '/' . $hdr_pattern . '/', $buff );
				fclose ( $fr );
			}
		}
		
		return $result;
	}
	/**
	 * When an archive is uploaded/downloaded via PHP you might find that it is prepended by one/many CRLF chars.
	 * This may be due the fact that some of your .php file (executed at runtime) if prepended by one/many CRLF chars
	 * before|after the `<?php`|`?>` statement(s)
	 *
	 * @param unknown $filename        	
	 * @param string $method        	
	 * @return boolean
	 */
	public function fixArchiveCRLF($filename, $method = null) {
		if ($this->isValidArchive ( $filename, $method ))
			return true;
		
		$result = false;
		if (file_exists ( $filename ) && false !== ($fr = fopen ( $filename, 'rb' ))) {
			
			// detect the number of #0D/0A chars to be skipped
			$buff = fread ( $fr, TAR_BUFFER_LENGTH );
			$offset = 0;
			while ( false !== $buff && in_array( ord( substr( $buff, $offset, 1 ) ), array( 10, 13 ) ) )
				$offset ++;
			fclose ( $fr );
			
			// if #0D/0A chars found then create a new file by skipping the necessary bytes ($offset)
			if ($offset > 0) {
				$b = basename ( $filename );
				$newname = str_replace ( $b, 'fixing-' . $b, $filename );
				$in = fopen ( $filename, 'rb' );
				$out = fopen ( $newname, 'wb' );
				
				false !== $in && false !== $out && $this->_pipeStreams ( $in, $out, - 1, $offset );
				false !== $in && fclose ( $in );
				false !== $out && fclose ( $out );
				$result = $this->isValidArchive ( $newname, $method );
				$result && $result = move_file ( $newname, $filename );
				$result && $this->_stdOutput( 
					sprintf( 
						_esc( 
							'[!] Archive %s has %d invalid bytes (0D|0A) prepended. I fixed it for now but this should never happen.' ), 
						$filename, 
						$offset ) );
			}
		}
		return $result;
	}
	/**
	 * Set the archive comment
	 *
	 * @param string $comment        	
	 * @return boolean Returns true on success, false otherwise
	 */
	public function setArchiveComment($comment) {
		return false;
	}
	protected function setDefaultComment() {
		$comment = 'Created with ' . ( defined( 'WPMYBACKUP' ) ? WPMYBACKUP : '' ) .
			 ( defined( 'APP_VERSION_NO' ) ? ' v' . APP_VERSION_NO : '' );
		$this->setArchiveComment( $comment );
	}
	/**
	 * Set the archive password
	 *
	 * @param string $password        	
	 * @return boolean Returns true on success, false otherwise
	 */
	public function setArchivePassword($password) {
		return false;
	}

	/**
	 * Open the archive
	 *
	 * @param string $filename
	 * @return boolean Return true on success, false otherwise
	 */
	public function open( $filename = null ) {
		$this->setFileName( $filename );
		return true;
	}

	/**
	 * Close the archive
	 *
	 * @return boolean Return true on success, false otherwise
	 */
	public function close() {
		return true;
	}
}
?>