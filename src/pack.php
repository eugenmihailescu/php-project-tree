<?php
define( 'MB', 1048576 );
define( 'NONE', 0 );
define( 'GZ', 1 );
define( 'BZ2', 2 );

global $COMPRESSION_NAMES, $COMPRESSION_FILTERS;

$COMPRESSION_NAMES = array( NONE => 'tar', GZ => 'gz', BZ2 => 'bz2' );

$COMPRESSION_FILTERS = array( NONE => array(), GZ => array( 'gz', 'wb%d' ), BZ2 => array( 'bz', 'w' ) );

function _get_ns_function_name( $function_name ) {
	return __NAMESPACE__ . '\\' . $function_name;
}

// Wrappers for some PHP functions based on namespace
function _function_exists( $function_name ) {
	return function_exists( _get_ns_function_name( $function_name ) );
}

/**
 * A is_callable wrapper that is namespace aware
 *
 * @param string $function_name
 * @return boolean
 */
function _is_callable( $function_name ) {
	return is_callable( is_string( $function_name ) ? _get_ns_function_name( $function_name ) : $function_name );
}

/**
 * A call_user_func wrapper that is namespace aware
 *
 * @param unknown $function_name
 * @return mixed
 */
function _call_user_func( $function_name ) {
	$args = array_slice( func_get_args(), 1 ); // remember, the first argument is the function name
	return call_user_func_array( 
		is_string( $function_name ) ? _get_ns_function_name( $function_name ) : $function_name, 
		$args );
}

/**
 * Extract the Git|SVN version from the repository specified by $path
 *
 * @param string $path
 * @return string|boolean Returns the version on success, false otherwise
 */
function _get_version( $path ) {
	$output = null;
	exec( 'git -C ' . $path . ' status', $output, $return_var );
	
	// if Git repository
	if ( 0 == $return_var ) {
		$result = exec( 'git -C ' . $path . ' describe --abbrev=0', $output, $return_var );
		if ( 0 == $return_var )
			return $result;
	} else {
		exec( 'svn status ' . $path, $output, $return_var );
		// if SVN repository
		if ( 0 == $return_var ) {
			$result = exec( 
				'svn info ' . $path . '|grep "Revision"|sed s/Revision: \\([0-9]*\\)/\1/', 
				$output, 
				$return_var );
			if ( 0 == $return_var )
				return $result;
		}
	}
	
	return false;
}

/**
 * Return the commit date for a SVN|Git repository at $path
 *
 * @param string $path The SVN|Git repository path
 * @return string|boolean Returns the date string on success, false on error
 */
function _get_commit_date( $path ) {
	$output = null;
	exec( 'git -C ' . $path . ' status', $output, $return_var );
	
	// if Git repository
	if ( 0 == $return_var ) {
		$result = exec( 'git -C ' . $path . ' log -1 --format=%ct', $output, $return_var );
		if ( 0 == $return_var )
			return $result;
	} else {
		exec( 'svn status ' . $path, $output, $return_var );
		// if SVN repository
		if ( 0 == $return_var ) {
			$result = exec( 
				'svn info ' . $path . '|grep "Last Changed Date"|sed s/Last Changed Date: \\([0-9\\-]+\\).*/\1/', 
				$output, 
				$return_var );
			if ( 0 == $return_var )
				return $result;
		}
	}
	
	return false;
}

/**
 * Creates the install.json file
 *
 * @param array $files
 * @param array $obj
 * @return string Returns the JSON string
 */
function get_install_json( &$files, &$obj ) {
	$fields = array( 
		'proj' => '', 
		'pack_path' => addslashes( __DIR__ ), 
		'proj_name' => '', 
		'group' => 'default', 
		'desc' => '', 
		'uri' => '', 
		'lib' => '', 
		'path' => '', 
		
		'author' => '', 
		'description' => '', 
		'requires' => '', 
		'tested_ver' => '', 
		'donate' => '', 
		'contributors' => '', 
		'sections' => '', 
		'license' => '', 
		'tags' => '', 
		'banners' => '' );
	
	$addon_info = array( 'files' => array() );
	foreach ( $fields as $field => $default ) {
		$$field = isset( $obj[$field] ) ? $obj[$field] : $default;
		$addon_info[$field] = isset( $obj[$field] ) ? $obj[$field] : $default;
	}
	
	$path .= DIRECTORY_SEPARATOR != substr( $path, - 1 ) ? DIRECTORY_SEPARATOR : '';
	
	$output_filename = $pack_path . ( DIRECTORY_SEPARATOR != substr( $pack_path, - 1 ) ? DIRECTORY_SEPARATOR : '' ) .
		 $proj;
	
	$addon_info = array( 
		'slug' => $proj, 
		'files' => array(), 
		'name' => $desc, 
		'homepage' => $uri, 
		'mainfile' => $lib, 
		'version' => _get_version( $path ), 
		'author' => $author, 
		'description' => $description, 
		'requires' => $requires, 
		'tested' => $tested_ver, 
		'donate_link' => $donate, 
		'contributors' => $contributors, 
		'sections' => $sections, 
		'license' => $license, 
		'tags' => $tags, 
		'last_updated' => _get_commit_date( $path ), 
		'license' => $license, 
		'banners' => $banners, 
		'files' => getProjectFiles( $path, $obj['files'] ) );
	
	return json_encode( $addon_info );
}

/**
 * Create a compressed tar.bz2 archive of the $files to $output_filename
 *
 * @param array $files An array containing the relative (to the project root) filenames
 * @param array $obj An array containing the project info
 * @param string $output_filename The TAR output archive name
 * @param string $files_root The project root path
 * @return string Returns the absolute path of the tar.bz2|zip archive
 */
function tar_it( &$files, &$obj, $output_filename, $files_root ) {
	require_once __DIR__ . DIRECTORY_SEPARATOR . 'class/TarArchive.php';
	require_once __DIR__ . DIRECTORY_SEPARATOR . 'class/MyZipArchive.php';
	
	$files_root .= DIRECTORY_SEPARATOR != substr( $files_root, - 1 ) ? DIRECTORY_SEPARATOR : '';
	
	$class_name = __NAMESPACE__ . '\\' .
		 ( isset( $obj['arc_type'] ) && 'zip' == $obj['arc_type'] ? 'MyZipArchive' : 'TarArchive' );
	
	$arc = new $class_name( $output_filename );
	
	foreach ( $files as $filename ) {
		$arc->addFile( $files_root . $filename, $filename );
	}
	
	$has_json = array_reduce( 
		$files, 
		function ( $carry, $item ) {
			return $carry || 'install.json' == basename( $item );
		}, 
		false );
	$addon_file = false;
	if ( ! $has_json ) {
		$addon_file = tempnam( sys_get_temp_dir(), 'addon' );
		file_put_contents( $addon_file, get_install_json( $files, $obj ) );
		$arc->addFile( $addon_file, 'install.json' );
	}
	$dest = $arc->compress( BZ2, 9 ); // in case of zip the arguments are just ignored
	
	$arc->close();
	
	( 'MyZipArchive' == $class_name ) || $arc->unlink();
	
	// clean-up
	is_file( $addon_file ) && unlink( $addon_file );
	
	if ( 'addon' == $obj['arc_type'] ) {
		
		$new_name = preg_replace( '/(.*\.)(zip|tar)/', '$1addon', $dest );
		copy( $dest, $new_name ) && $dest = $new_name;
	}
	
	return $dest;
}

/**
 * Creates the install.json file
 *
 * @param array $files The files as returned by getProjectFiles
 * @param array $obj The object as returned by getProjectData
 * @param string $filename The filename where to save the JSON file (default to temp dir)
 * @param string $version The version to embed into JSON (default to `0.1`)
 * @return string Returns the full path of the resulted package file
 */
function installJSON( &$files, &$obj, $filename = '', $version = '' ) {
	$addon_file = empty( $filename ) ? sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'install.json' : $filename;
	$json_content = get_install_json( $files, $obj );
	if ( ! empty( $version ) ) {
		if ( $json_array = json_decode( $json_content, true ) )
			$json_array['version'] = $version;
		$json_content = json_encode( $json_array );
	}
	return file_put_contents( $addon_file, $json_content );
}

/**
 * Creates a complete installation package for the specified project
 *
 * @param array $files The files as returned by getProjectFiles
 * @param array $obj The object as returned by getProjectData
 * @param string $pack_path The custom path where to deploy the package. If FALSE then the package default path.
 * @return string Returns the full path of the resulted package file
 */
function packFiles( &$files, &$obj, $pack_path = false ) {
	$root = $obj['path'];
	$root .= DIRECTORY_SEPARATOR != substr( $root, - 1 ) ? DIRECTORY_SEPARATOR : '';
	
	$pack_path || $pack_path = $obj['pack_path'];
	$output_filename = $pack_path . ( DIRECTORY_SEPARATOR != substr( $pack_path, - 1 ) ? DIRECTORY_SEPARATOR : '' ) .
		 $obj['proj'];
	
	$tmpdir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid( 'pack' ) . DIRECTORY_SEPARATOR;
	mkdir( $tmpdir, 0770, true );
	
	// this is a custom PHP script that inject a GPLv3 header at the begining of each .php|.css|.js file
	$script_name = dirname( dirname( __DIR__ ) ) . DIRECTORY_SEPARATOR .
		 'wordpress.plugins/wpmybackup/build/inc/php/php-inject-glpv3.php';
	
	// insert a GPLv3 header
	if ( $insert_gplv3_header = is_file( $script_name ) ) {
		$inject_params = array( 
			'd' => $tmpdir, 
			'c' => $tmpdir, 
			'g' => $root, 
			't' => 'WP MyBackup', 
			'a' => 'Eugen Mihailescu', 
			'u' => 'http://wpmybackup.mynixworld.info', 
			'k' => 'eugenmihailescux@gmail.com', 
			'm' => null, 
			'n' => 'MyBackup', 
			'q' => null );
		foreach ( $files as $filename ) {
			$dir = dirname( $tmpdir . $filename );
			! file_exists( $dir ) && mkdir( $dir, 0770, true );
			copy( $root . $filename, $tmpdir . $filename );
		}
		$cmdline = 'php ' . $script_name;
		foreach ( $inject_params as $key => $value ) {
			$cmdline .= sprintf( ' -%s %s', $key, null !== $value ? "'$value'" : '' );
		}
		
		exec( $cmdline, $output, $result );
	}
	
	// remove existent destination file
	is_file( $output_filename ) && unlink( $output_filename );
	
	$output_filename = tar_it( $files, $obj, $output_filename, $tmpdir );
	
	_rmdir( $tmpdir, true );
	
	return is_file( $output_filename ) ? $output_filename : false;
}

/**
 * Extract the files from the $input_filename package to the $output_path directory
 *
 * @param string $input_filename
 * @param string $output_path
 * @return bool|array Returns false if invalid archive otherwise returns an array where the key=source filename and the
 *         value=temporary extracted filename
 */
function unpackFile( $input_filename, $output_path ) {
	require_once 'class/TarArchive.php';
	$tar = new TarArchive( '' );
	$tar->setFileName( $input_filename );
	$tarfile = $tar->decompress();
	if ( file_exists( $tarfile ) ) {
		( $result = $tar->extract( $tarfile, $output_path ) ) && $tar->unlink();
		@unlink( $tarfile );
	}
	return $result;
}
