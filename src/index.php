<?php
require_once 'utils.php';
define( 'SANDBOX_MODE', false );
file_exists( STORAGE_PATH ) || mkdir( STORAGE_PATH );
$packlib = __DIR__ . DIRECTORY_SEPARATOR . 'pack.php';
define( 'PACKLIB', file_exists( $packlib ) );

function array_filter_recursive( &$array ) {
	foreach ( $array as $key => $item ) {
		is_array( $item ) && $array[$key] = array_filter_recursive( $item );
		if ( empty( $array[$key] ) )
			unset( $array[$key] );
	}
	return $array;
}

/**
 * Generates the HTML tree list of the project's files
 *
 * @param array $array The files of the project
 * @param string $parent The tree parent (empty string by default)
 * @param number $level The tree identation level (0 by default)
 * @param array $sel_files An array of the selected/checked files within $array
 */
function printTree( &$array, $parent = '', $level = 0, $sel_files = null ) {
	$tab = 20;
	foreach ( $array as $key => $children ) {
		$style = $level > 0 ? ' style="padding-left:' . ( $level * $tab ) . 'px"' : '';
		$id = is_array( $children ) ? $key : $children;
		$name = $id;
		
		// this encloses parent+filename (hopefully it's unique)
		$id = ( empty( $parent ) ? '' : $parent . '.' ) . base_convert( crc32( $id ), 10, 32 );
		
		$sel = is_array( $sel_files ) && in_array( $id, $sel_files );
		$checked = $sel ? ' checked' : '';
		
		if ( preg_match( '/.*\.(php|fix|txt)$/', $name ) ) {
			$dep = preg_match( '/.*\.(php|fix)$/', $name ) ? sprintf( 
				'<img src="img/deps.png" title="View file dependencies/uses" style="display:%s;cursor:pointer;width:16px;height:18px;float:right" onclick="do_action(\'lst_dependency\',\'proj=\'+document.getElementById(\'proj_name\').value+\'&id=%s\');">', 
				$sel ? 'inline-block' : 'none', 
				$id ) : '';
			$dep .= sprintf( 
				'<img src="img/diff.png" title="View file includes" style="display:%s;cursor:pointer;width:16px;height:18px;float:right" onclick="do_action(\'file_view\',\'proj=\'+document.getElementById(\'proj_name\').value+\'&id=%s\');">', 
				$sel ? 'inline-block' : 'none', 
				$id );
		} else
			$dep = '';
		printf( 
			'<div class="row"%s><input type="checkbox" id="%s" onclick="balance_checkboxes(this);"%s> %s%s</div>', 
			$style, 
			$id, 
			$checked, 
			$name, 
			$dep );
		is_array( $children ) && printTree( $children, $id, $level + 1, $sel_files );
	}
}

/**
 * Checks the project file dependency.
 * It outputs a semi-column delimited file list.
 * This function is used via Ajax so this exits the script
 *
 * @param array $array The project's files
 * @param string $id The encoded file ID
 * @param string $used_by When true returns the files that uses the file mentioned by ID, otherwise the files required to run the file by ID
 */
function printDeps( &$array, $id = null, $used_by = false ) {
	$path = $array['path'];
	
	// decode the files as stored withing .json file
	$dirlist = getFiles( $path );
	$dirlist1 = $dirlist;
	$path = dirname( $path ) . DIRECTORY_SEPARATOR;
	array_walk( $dirlist1, function ( &$item ) use(&$path ) {
		$item = str_replace( $path, '', $item );
	} );
	
	if ( ! $used_by ) {
		$files = explode( ',', empty( $id ) ? $array['files'] : $id );
		
		$files = decode_filenames( $files, $dirlist1, false );
	} else
		$files = array( $id ); // already decoded filename
			                       
	// calculate the dependencies of those files
	$deps = get_dependencies( $files, $dirlist, $used_by );
	
	// prepare the result
	$result = array_diff( $deps, array_flip( $files ) );
	$dirlist = array_filter( $dirlist, function ( $a ) use(&$result ) {
		return isset( $result[basename( $a )] );
	} );
	$dirlist = array_flip( $dirlist );
	array_walk( $dirlist, function ( &$item, $key ) {
		$item = basename( $key );
	} );
	$dirlist = array_flip( $dirlist );
	
	// encode the result
	array_walk( 
		$dirlist, 
		function ( &$item ) use(&$path ) {
			$item = encode_filename( str_replace( $path, '', $item ) );
		} );
	
	$result = array();
	foreach ( $dirlist as $file => $encoded_path )
		$result[] = $encoded_path . '=' . $deps[$file];
		
		// sort the output alphabetically
	uasort( 
		$result, 
		function ( $a, $b ) {
			$a = explode( ',', $a );
			$b = explode( ',', $b );
			return end( $a ) > end( $b );
		} );
	
	echo implode( ';', $result );
}

/**
 * Generates the deployment package for the project given by $obj.
 * When $_REQUEST contains 'install-json' it generates the install.json file at the path specified by $_REQUEST['json-path'] otherwise at system temporary directory
 * When $_REQUEST does not contain 'install-json' it generates the package tar.bz2|zip arhive that includes the necessary files to run the PHP project
 *
 * @param object $obj
 * @param string $pack_path The custom path where to deploy the package. If FALSE then the package default path.
 *       
 * @return Returns true or the .tar.bz2|zip archive name in case of generating successfully the archive, false on error
 */
function createPack( &$obj, $pack_path = false ) {
	// decode the files as stored withing .json file
	$files = getProjectFiles( $obj['path'], $obj['files'] );
	$packlib = __DIR__ . DIRECTORY_SEPARATOR . 'pack.php';
	
	// the request may contain 1-2 additional params to create the install.json only:
	// install-json : this tells to create the install.json only
	// json-path : optionally this tell where to create the install.json file (if not provided then on temp dir)
	// json-ver: optionally the version number (default to `0.1`)
	// Ex: curl deploy.localhost -d action=pack -d proj=<proj-slug> -d type=0 -d install-json=1 -d json-path=<path> -d
	// json-ver=1.2.3
	if ( is_file( $packlib ) ) {
		include_once $packlib;
		$json_path = isset( $_REQUEST['json-path'] ) ? $_REQUEST['json-path'] : null;
		$json_ver = isset( $_REQUEST['json-ver'] ) ? $_REQUEST['json-ver'] : null;
		
		if ( isset( $_REQUEST['install-json'] ) )
			return installJSON( $files, $obj, $json_path, $json_ver );
		else
			return packFiles( $files, $obj, $pack_path );
	}
	
	return false;
}

// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// MAIN: process the requested actions
// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

if ( isset( $_REQUEST ) && isset( $_REQUEST['action'] ) ) {
	! isset( $_REQUEST['proj'] ) || $obj = getProjectData( $_REQUEST['proj'], true );
	$sel_files = null;
	$fields = array( 
		'proj' => '', 
		'pack_path' => addslashes( __DIR__ ), 
		'group' => 'default', 
		'desc' => '', 
		'uri' => '', 
		'lib' => '', 
		'files' => '', 
		'path' => '', 
		'arc_type' => 'tarbz', 
		'tested' => 0, 
		'author' => '', 
		'description' => '', 
		'requires' => '', 
		'tested_ver' => '', 
		'donate' => '', 
		'contributors' => '', 
		'sections' => '', 
		'license' => '', 
		'banners' => '', 
		'tags' => '' );
	
	// \\\\\\\\\\\\\\\ EDIT //////////////////
	if ( ( $edit = 'edit' == $_REQUEST['action'] ) && $obj ) {
		$sel_files = explode( ',', $obj['files'] );
		// $path = $obj ['path'];
		
		$tested = isset( $obj['tested'] ) && $obj['tested'];
		
		foreach ( $fields as $field => $default )
			isset( $obj[$field] ) && ( $$field = $obj[$field] ) || $$field = $default;
	}
	
	// \\\\\\\\\\\\\\\ PROJECT LIST //////////////////
	if ( $edit || 'scan' == $_REQUEST['action'] && isset( $_REQUEST['path'] ) ) {
		$path = $edit ? $path : $_REQUEST['path'];
		'.' == substr( $path, 0, 1 ) && $path = __DIR__ . substr( $path, 1 );
		
		if ( file_exists( $path ) && ( ! SANDBOX_MODE || false !== strpos( $path, __DIR__ ) ) ) {
			
			$files = getFiles( $path, true );
			array_filter_recursive( $files );
			array_sort_recursive( $files );
			$count = count( $files, true );
			$selcount = 0;
			$edit && ob_start();
			printTree( $files, '', 0, $sel_files );
			$edit && $files = ob_get_clean();
		} else {
			$count = 0;
			$files = array();
			if ( ! $edit )
				echo 'Path does not exist or access denied';
		}
		$edit || exit();
	}
	
	// \\\\\\\\\\\\\\\ SAVE //////////////////
	if ( 'save' == $_REQUEST['action'] && isset( $_REQUEST['files'] ) && isset( $_REQUEST['path'] ) ) {
		
		foreach ( $fields as $field => $default )
			$$field = isset( $_REQUEST[$field] ) ? $_REQUEST[$field] : $default;
		
		$path = '.' == substr( $path, 0, 1 ) ? __DIR__ . substr( $path, 1 ) : $path;
		
		file_exists( $path ) || die( sprintf( 'The specified path "%s" does not exist', $path ) );
		
		$obj = array( 'timestamp' => time() );
		foreach ( array_keys( $fields ) as $field )
			$obj[$field] = $$field;
		
		$saved = false !== file_put_contents( STORAGE_PATH . $proj . '.' . STORAGE_EXT, json_encode( $obj ) );
		die( $saved );
	}
	
	// \\\\\\\\\\\\\\\ REMOVE //////////////////
	if ( 'remove' == $_REQUEST['action'] && $obj ) {
		if ( unlink( STORAGE_PATH . $_REQUEST['proj'] ) )
			echo encode_filename( $_REQUEST['proj'] );
			// echo $_REQUEST ['proj'];
		else
			echo 'The project ' . $obj['proj'] . ' couldn\'t be removed. It does not exists or access denied.';
		die();
	}
	
	// \\\\\\\\\\\\\\\ EDIT //////////////////
	if ( $edit && $obj ) {
		$obj['files'] = $files;
		$obj['filescount'] = $count;
		$obj['selcount'] = count( $sel_files, 1 );
		$obj['path'] = str_replace( __DIR__, '.', $obj['path'] );
		! file_exists( $obj['path'] ) && $obj['path'] = './';
		
		echo json_encode( $obj );
		
		die();
	}
	// \\\\\\\\\\\\\\\ CHECK PROJECT DEPENDENCIES //////////////////
	if ( ( 'chk_dependency' == $_REQUEST['action'] || 'lst_dependency' == $_REQUEST['action'] ||
		 'lst_usedby' == $_REQUEST['action'] ) && $obj ) {
		$file_id = isset( $_REQUEST['id'] ) ? $_REQUEST['id'] : null;
		printDeps( $obj, $file_id, 'lst_usedby' == $_REQUEST['action'] );
		die();
	}
	
	// \\\\\\\\\\\\\\\ EXPORT //////////////////
	if ( ( 'export' == $_REQUEST['action'] || 'autoload' == $_REQUEST['action'] ) && $obj )
		if ( ! headers_sent() ) {
			
			$type = isset( $_REQUEST['type'] ) && 'false' != $_REQUEST['type'];
			
			$autoloader = 'autoload' == $_REQUEST['action'];
			
			// decode the files as stored withing .json file
			$files = getProjectFiles( $obj['path'], $obj['files'] );
			
			// output the files list for download
			if ( $autoloader )
				$content = implode( PHP_EOL, getProjectAutoloader( $obj['path'], $files, $type ) );
			else
				$content = implode( $type ? ',' : PHP_EOL, $files );
			
			downloadFileContent( 
				sprintf( '%s%s.%s"', $_REQUEST['proj'], $autoloader ? '-autoloader' : '', $autoloader ? 'php' : 'txt' ), 
				$content, 
				'text/plain' );
		} else
			echo 'Cannot send the content because header already sent';
		
		// \\\\\\\\\\\\\\\ PACK //////////////////
	if ( 'pack' == $_REQUEST['action'] && isset( $obj ) ) {
		( $pack = createPack( $obj ) ) && isset( $_REQUEST['type'] ) && $_REQUEST['type'] &&
			 downloadFile( $pack, 'application/zip' );
	}
	
	// \\\\\\\\\\\\\\\ DEPLOY //////////////////
	if ( 'deploy' == $_REQUEST['action'] && isset( $_REQUEST['projlist'] ) && isset( $_REQUEST['path'] ) ) {
		// decode the files as stored withing .json file
		$deploy_path = $_REQUEST['path'] .= DIRECTORY_SEPARATOR == substr( $_REQUEST['path'], - 1 ) ? '' : DIRECTORY_SEPARATOR;
		is_dir( $deploy_path ) || mkdir( $deploy_path, 0770, true );
		$proj_list = explode( ',', $_REQUEST['projlist'] );
		array_walk( $proj_list, function ( &$item, $key ) {
			$item = substr( $item, 4 );
		} );
		$failed = array();
		foreach ( $proj_list as $proj ) {
			null != ( $obj = getProjectData( $proj, true ) ) ||
				 $failed[] = sprintf( 'Cannot decode the project id %s', $proj );
			( $pack = $obj ? createPack( $obj, $_REQUEST['path'] ) : false ) || $failed[] = sprintf( 
				'Could not pack the project %s to the location %s', 
				$obj['proj'], 
				$obj['pack_path'] );
			/*
			 * ! empty( $pack ) && is_file( $pack ) && unpackFile( $pack, $deploy_path ) || $failed[] = sprintf( 'Could not deploy the file %s to the location %s', $pack,
			 * $deploy_path );
			 */
		}
		count( $failed ) > 0 && die( implode( '<br>', $failed ) ) || die();
	}
	
	// \\\\\\\\\\\\\\\ GRAPH //////////////////
	if ( ( 'graph' == $_REQUEST['action'] || 'graph_file' == $_REQUEST['action'] ) && $obj ) {
		// @see http://stackoverflow.com/questions/7034/graph-visualization-library-in-javascript
		$path = $obj['path'];
		
		// decode the files as stored withing .json file
		$dirlist = getFiles( $path );
		
		if ( ! isset( $_REQUEST['id'] ) ) {
			$dirlist1 = $dirlist;
			$path = dirname( $path ) . DIRECTORY_SEPARATOR;
			array_walk( 
				$dirlist1, 
				function ( &$item ) use(&$path ) {
					$item = str_replace( $path, '', $item );
				} );
			
			$files = explode( ',', empty( $id ) ? $obj['files'] : $id );
			$files = decode_filenames( $files, $dirlist1, false );
		} else
			$files = array( $_REQUEST['id'] );
		
		try {
			ini_set( 'max_execution_time', 0 );
			
			$graph_deps = graph_dependencies( $files, $dirlist );
			
			die( json_encode( $graph_deps ) );
		} catch ( Exception $e ) {
			die( $e->getMessage() );
		}
	}
	
	// \\\\\\\\\\\\\\\ FILE VIEW //////////////////
	if ( 'file_view' == $_REQUEST['action'] && isset( $_REQUEST['id'] ) && $obj ) {
		$path = $obj['path'];
		// decode the files as stored withing .json file
		$dirlist = getFiles( $path );
		$path = dirname( $path ) . DIRECTORY_SEPARATOR;
		array_walk( $dirlist, function ( &$item ) use(&$path ) {
			$item = str_replace( $path, '', $item );
		} );
		
		$buffer = file_get_contents( $path . decode_filename( $_REQUEST['id'], $dirlist ) );
		$buffer = preg_replace( 
			"/^\s*(require|include)(_once)?\s+[^'\";]*['\"].*\.php['\"].*/m", 
			'<span style="background-color:yellow">$0</span>', 
			htmlentities( $buffer ) );
		die( $buffer );
	}
	
	if ( 'send_feedback' == $_REQUEST['action'] ) {
		processSupportForm( $_REQUEST );
		die();
	}
}

// UI help messages
$help_path = "'Specify the root path of the project. You will be able to include only files contained below this path.'";
$help_slug = "'Specify a slug for your project. Make sure it does not overlap others slugs otherwise you will overwrite that project.'";
$help_name = "'Specify an user-friendly name for this project.'";
$help_uri = "'Specify the project home page URI.'";
$help_lib = "'Specify the project`s main file.'";
$help_pack = "'Specify the temporary path where the project .tar.bz2 package will be created by default.'";
$help_group = "'You may group your projects by a generic hierarchy. Enter here the name of the group that this project belongs to. Let the <b>default</b> value if you do not have any prefference.'";
$help_arc_type = "'Select tar.bz2 for non-WordPress projects, otherwise zip; addon is just a tar.bz2 with .addon extension'";
$help_author = "'Enter the name of the author.'";
$help_desc = "'Specify a brief description of the project.'";
$help_requires = "'Specify the minimal version of `environment` required to run this project.'";
$help_tested = "'Specify the maximum version of `environment` that this application was tested and works.'";
$help_donate_link = "'Specify the URI for donate link, if any.'";
$help_contributors = "'Enter a semicolon delimited list of contributors.<br><b>Example:</b><ul><li>contributor-id1;contributor-id2</li></ul>'";
$help_sections = "'Specify a semicolon delimited list of sections.<br><b>Example:</b><ul><li>section-title=section-description;section-title=section-description</li></ul>'";
$help_license = "'Specify the license version (eg. GPLv2, Proprietary, etc) for this project.'";
$help_tags = "'Specify a semicolon delimited list of tags for this project.<br><b>Example:</b><ul><li>tag-key=tag-name;tag-key=tag-name</li></ul>'";
$help_banners = "'Specify a semicolon delimited list of banners for this project.<br><b>Example:</b><ul><li>low=url-low-banner;high=url-high-banner</li></ul>'";

$projects = array();
foreach ( glob( STORAGE_PATH . '*.' . STORAGE_EXT ) as $proj_file ) {
	$obj = getProjectDataByFilename( $proj_file, true );
	$group = isset( $obj['group'] ) ? $obj['group'] : 'default';
	$tested = isset( $obj['tested'] ) && $obj['tested'];
	! isset( $projects[$group] ) && $projects[$group] = array();
	
	$projects[$group][basename( $proj_file )] = $tested;
}
include_once 'res/index.tpl';

?>
