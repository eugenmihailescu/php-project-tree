<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PHP project deployment</title>
<link rel="stylesheet" type="text/css" href="css/proj-deploy.css">
<script src="js/proj-deploy.js"></script>
</head>
<body>
	<div style="height: 40px; text-align: center"><?php echo SANDBOX_MODE?'<h3>Sandbox mode only</h3>':'';?></div>
	<div id="wrapper"
		style="max-width: 640px; margin-left: auto; margin-right: auto; background-color: white; outline: 10px solid #f8a283;">
		<div id="status" style="padding: 5px"></div>
		<table style="margin-left: auto; margin-right: auto;">
			<tr>
				<td colspan="5"><table id="tbl_proj" class='projlist'
						style="border: 1px solid #d0d0d0; border-radius: 10px; color: #00ADEE; width: 100%; overflow-y: auto">
						<tr>
							<td colspan="5"><?php if(empty($projects))echo 'No project found';?></td>
						</tr>
						<?php
						$old_group = null;
						foreach ( $projects as $group => $group_projects ) {
							$group_id = str_replace( ' ', '_', $group );
							if ( $old_group != $group ) {
								echo '<tr style="background-color:#00adee;color:white;font-weight:bold;text-align:center"><td><input type="checkbox" id="chk_', $group_id, '" style="display:none" data-isgroup="1" onclick="var items=document.getElementById(\'wrapper\').querySelectorAll(\'.group-', $group_id, '\'),i;for(i=0;i<items.length;i+=1)items[i].checked=this.checked;"></td><td colspan="4">', $group, '</td></tr>';
								$old_group = $group;
							}
							foreach ( $group_projects as $p => $tested ) {
								$id = encode_filename( $p );
								$data = json_decode( file_get_contents( STORAGE_PATH . $p ), true );
								printf( 
									'<tr id="t%s"><td><input type="checkbox" id="chk_%s" style="display:none" class="group-%s"></td><td><img class="btn_action"  src="img/edit.png" onclick="do_action(\'edit\',\'proj=%s\');" title="Edit project"></td><td><img class="btn_action" src="img/remove.png" onclick="if(confirm(\'Are you sure you want to remove it?\'))do_action(\'remove\',\'proj=%s\');" title="Delete project"></td><td title="%s">%s</td><td%s></td></tr>', 
									$id, 
									$p, 
									$group_id, 
									$p, 
									$p, 
									$data['description'], 
									sprintf( '<a href="%s" target="_blank">%s</a>', $data['uri'], $p ), 
									$tested ? ' style="min-width:16px;background:url(img/starred.png) no-repeat center;" title="Certified as tested"' : '' );
							}
						}
						?>
					</table></td>
			</tr>
			<tr id="proj_info_row">
				<td colspan="3">
					<div>
						<a id="version_info_lbl" style="color: #00ADEE; cursor: pointer;"
							onmouseover="this.style.color='#F8A283';"
							onmouseout="this.style.color='#00ADEE';" onclick="switch_options();">Version
							information >></a>
					</div>
					<div style="border: 1px solid #d0d0d0; border-radius: 10px">
						<table id="version_info_std">
							<tr>
								<td><label for="proj_path">PHP project root</label></td>
								<td colspan="4"><input id="proj_path" type="text" size="50"
									value="<?php echo '.'.DIRECTORY_SEPARATOR;?>"><a class='help'
									onclick=<?php echo echoHelp($help_path); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_name">Project slug</label></td>
								<td colspan="4"><input id="proj_name" type="text" size="50"
									placeholder="you-name-it"><a class='help'
									onclick=<?php echo echoHelp($help_slug); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_desc">Project name</label></td>
								<td colspan="4"><input id="proj_desc" type="text" size="50"
									placeholder="short description"><a class='help'
									onclick=<?php echo echoHelp($help_name); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_uri">Project home page</label></td>
								<td colspan="4"><input id="proj_uri" type="text" size="50"
									placeholder="http://"><a class='help'
									onclick=<?php echo echoHelp($help_uri); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_lib">Project main file</label></td>
								<td colspan="4"><input id="proj_lib" type="text" size="50"
									placeholder="file1,file2,.."><a class='help'
									onclick=<?php echo echoHelp($help_lib); ?>>[?]</a></td>
							</tr>
<?php if(PACKLIB){?>						
						<tr>
								<td><label for="pack_path">Package temp path</label></td>
								<td colspan="4"><input id="pack_path" type="text" size="50"
									value="<?php echo '.'.DIRECTORY_SEPARATOR;?>"><a class='help'
									onclick=<?php echo echoHelp($help_pack); ?>>[?]</a></td>
							</tr>
<?php }?>					
						<tr>
								<td><label for="proj_group">Porject group</label></td>
								<td colspan="4"><input id="proj_group" type="text" size="50"
									value="default"><a class='help'
									onclick=<?php echo echoHelp($help_group); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="arc_type">Package type</label></td>
								<td colspan="4"><select id="arc_type">
										<option value="addon">addon</option>
										<option value="tarbz">tar.bz2</option>
										<option value="zip">zip</option>
								</select><a class='help' onclick=<?php echo echoHelp($help_arc_type); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="lbl_filecount">Files count</label></td>
								<td><span id="lbl_filecount"></span></td>
								<td><label for="lbl_selcount">Selected</label></td>
								<td><span id="lbl_selcount"></span></td>
								<td><input type="checkbox" id="chk_tested" value="0"><label
									for="chk_tested">Tested</label></td>
							</tr>
						</table>
						<table id="version_info_adv" style="display: none">
							<tr>
								<td><label for="proj_author">Author</label></td>
								<td colspan="4"><input id="proj_author" type="text" size="50"
									placeholder="Eugen Mihailescu"><a class='help'
									onclick=<?php echo echoHelp($help_author); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_description">Description</label></td>
								<td colspan="4"><input id="proj_description" type="text" size="50"
									placeholder="brief description"><a class='help'
									onclick=<?php echo echoHelp($help_desc); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_requires">Requires</label></td>
								<td colspan="4"><input id="proj_requires" type="text" size="50"
									placeholder="eg. 3.3.0"><a class='help'
									onclick=<?php echo echoHelp($help_requires); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_tested">Tested</label></td>
								<td colspan="4"><input id="proj_tested" type="text" size="50"
									placeholder="eg. 4.3"><a class='help'
									onclick=<?php echo echoHelp($help_tested); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_donate_link">Donate link</label></td>
								<td colspan="4"><input id="proj_donate_link" type="text" size="50"
									placeholder="http://"><a class='help'
									onclick=<?php echo echoHelp($help_donate_link); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_contributors">Contributors</label></td>
								<td colspan="4"><input id="proj_contributors" type="text" size="50"
									placeholder="user1=name1;user2=name2;..."><a class='help'
									onclick=<?php echo echoHelp($help_contributors); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_sections">Sections</label></td>
								<td colspan="4"><input id="proj_sections" type="text" size="50"
									placeholder="section1=description1;section2=description2;..."><a
									class='help' onclick=<?php echo echoHelp($help_sections ); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_license">License</label></td>
								<td colspan="4"><input id="proj_license" type="text" size="50"
									placeholder="eg. GPLv3"><a class='help'
									onclick=<?php echo echoHelp($help_license ); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_tags">Tags</label></td>
								<td colspan="4"><input id="proj_tags" type="text" size="50"
									placeholder="tagkey1=tagvalue1;tagkey2=tagvalue2;..."><a class='help'
									onclick=<?php echo echoHelp($help_tags); ?>>[?]</a></td>
							</tr>
							<tr>
								<td><label for="proj_banners">Banners</label></td>
								<td colspan="4"><input id="proj_banners" type="text" size="50"
									placeholder="low=low-banner;high=high-banner"><a class='help'
									onclick=<?php echo echoHelp($help_banners); ?>>[?]</a></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="5" style="text-align: center"><input class="button"
					type="button" id="btn_add" value="Add new" onclick="add_project('add');"
					title="Add a new project definition"> <input class="button-primary"
					type="button" id="btn_deploy" value="Deploy"
					title="Pack and deploy a project" onclick="add_project('deploy');"> <input
					id="btn_next" class="button" type="button" value="Next"
					onclick="add_project('next');"><input id="btn_cancel" class="button"
					type="button" value="Cancel" onclick="add_project('cancel');"
					title="Discard the changes"><input id="btn_save" class="button"
					type="button" value="Save" onclick="add_project('save');"
					title="Save the changes"><input class="button" type="button"
					id="btn_fixdeps" value="Fix dependency" onclick="fix_deps();"
					title="Include also the files marked as `missed`"><input class="button"
					type="button" id="btn_deps" value="Check dependency"
					title="Check what other files should be included"
					onclick="do_action('chk_dependency','proj='+document.getElementById('proj_name').value);"><span
					id="lbl_deps" style="background-color: yellow"></span></td>
			</tr>
			<tr>
				<td colspan="5" style="text-align: center"><input id="btn_graph"
					class="button" type="button" value="Graph"
					title="Display the project files dependency tree"
					onclick="do_action('graph','proj='+document.getElementById('proj_name').value);">
					<input id="btn_list" class="button" type="button" value="File list"
					title="Display the project files list"
					onclick="do_action('edit','proj='+document.getElementById('proj_name').value);">
					<input type="button" class="button" id="btn_export" value="Export"
					title="Export the used file list"
					onclick="var i=confirm('Export as a Ant compliant FileList ?');do_action('export','proj='+document.getElementById('proj_name').value+'&type='+i,false);">
					<input type="button" class="button" id="btn_aloader" value="Autoloader"
					title="Generate a PHP autoloader script"
					onclick="var i=confirm('Do you want me to generate the class path constants too?');do_action('autoload','proj='+document.getElementById('proj_name').value+'&type='+i,false);">
					<?php if(PACKLIB){?><input type="button" class="button-primary"
					id="btn_pack" value="Pack+Copy"
					title="Create a self-contained deployment package"
					onclick="var i=confirm('Do you want also to download the package?');do_action('pack','proj='+document.getElementById('proj_name').value+'&type='+i,!i);">
					<?php }?>
					</td>
			</tr>
		</table>
		<div id="file_list" class="files_list"></div>
	</div>
	<div id='footer' class='footer'>
		<abbr title='Copyright 2015'>&copy;</abbr> Eugen Mihailescu | <a
			href='mailto:eugenmihailescux@gmail.com'>Contact</a> | <a target='_blank'
			href='https://bitbucket.org/eugenmihailescu/php-project-tree'>Source</a>
	</div>
	<?php include_once 'feedback.tpl';?>
	<script>
		onload=function(){upd_btn_status();}
	</script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<script src="js/popup.js"></script>
	<script src="js/springy/springy.js"></script>
	<script src="js/springy/springyui.js"></script>
	<script src="js/graph.js"></script>
	<script src="js/feedback.js"></script>
	<script type="text/javascript">
function switch_options(){
	var tbl1=document.getElementById('version_info_std'),tbl2=document.getElementById('version_info_adv'),version_info_lbl=document.getElementById('version_info_lbl');
	if(tbl1.style.display=="none"){
		tbl1.style.display="block";
		tbl2.style.display="none";
		version_info_lbl.innerHTML="Version information&nbsp;&gt;&gt;";
	}
	else
	{
		tbl1.style.display="none";
		tbl2.style.display="block";
		version_info_lbl.innerHTML="&lt;&lt;&nbsp;Basic information";
	}
}
	</script>
</body>
</html>
