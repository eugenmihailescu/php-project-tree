<div class='feedback-box' id='feedback_box' style='border-radius: 0px;'>
	<span style='padding: 5px; font-size: 1.5em; font-weight: bold;'>Feedback
		form</span>
	<div class='feedback-btn' id='feedback_btn'
		onclick='toggle_feedbackbox();'>
		<img src='img/bullhorn.png' />&nbsp;Feedback

	</div>
	<table style='width: 100%; padding: 10px;'>
		<tr>
			<td>Your name:</td>
			<td><input id='sender' type="text" style='width: 100%'></td>
			<td style='padding: 5px; color: #f00; text-align: center;'>*</td>
		</tr>
		<tr>
			<td>Email address:</td>
			<td><input id='email' type="email" style='width: 100%'></td>
			<td style='padding: 5px; color: #f00; text-align: center;'>*</td>
		</tr>
		<tr>
			<td colspan="3">Your message:</td>
		</tr>
		<tr>
			<td colspan="3"><textarea id='body'
					style='width: 100%; height: 10em; resize: none;'></textarea></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" style='text-align: center'><div class='button'
					style='display: inline; margin-left: auto; margin-right: auto'
					onclick='sendFeedback();'>Send your message</div></td>
		</tr>
	</table>
</div>